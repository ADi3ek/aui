module.exports = function(karma) {
    var config = require('./build/configs/karma-shared');
    config.singleRun = false;
    config.autoWatch = true;
    karma.set(config);
};
