module.exports = function (grunt) {
    'use strict';

    var config = require('./build/configs');

    config.jquery = grunt.option('jquery') || '1.8.3';
    config.pkg = grunt.file.readJSON('package.json');

    grunt.initConfig(config);
    grunt.loadTasks('build/tasks');
    grunt.loadNpmTasks('grunt-available-tasks');
    grunt.loadNpmTasks('grunt-contrib');
    grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('build', 'Builds AUI.', ['clean:dist', 'clean:tmp', 'concat', 'cssmin', 'uglify', 'copy', 'clean:tmp']);
    grunt.registerTask('default', 'Shows the available tasks in a much more user friendly manner.', 'availabletasks');
    grunt.registerTask('docs', 'Builds the documentation and starts a server.', ['connect:docs']);
    grunt.registerTask('test', 'Runs the unit tests.', ['requirejs-config', 'karma:cli', 'clean:tmp']);
    grunt.registerTask('test-all', 'Cleans, installs, runs unit tests and integration tests. Generally, only the build server will be executing this task.', ['install', 'test', 'integration-tests']);
};
