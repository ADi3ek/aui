(function($) {
    
    var $overflowEl;

    AJS.dim = function (useShim, zIndex) {
        // IE-old needs the overflow on the HTML element so scrollbars are hidden
        // lazy-loaded because body does not exist when scripts are loaded in the head.
        if (!$overflowEl) {
            $overflowEl = $.browser.msie && parseInt($.browser.version,10) < 8 ?
                $('html') :
                $(document.body);
        }

        if (!AJS.dim.$dim) {
            AJS.dim.$dim = AJS("div").addClass("aui-blanket");
            if (zIndex) {
                AJS.dim.$dim.css({zIndex: zIndex});
            }
            if ($.browser.msie) {
                AJS.dim.$dim.css({width: "200%", height: Math.max($(document).height(), $(window).height()) + "px"});
            }
            $("body").append(AJS.dim.$dim);

            // Add IFrame shim
            if ($.browser.msie && (useShim !== false)) {
                AJS.dim.$shim = $('<iframe frameBorder="0" class="aui-blanket-shim" src="about:blank"/>');
                AJS.dim.$shim.css({height: Math.max($(document).height(), $(window).height()) + "px"});
                if (zIndex) {
                    AJS.dim.$shim.css({zIndex: zIndex - 1});
                }
                $("body").append(AJS.dim.$shim);
                // AJS.dim.shim is a legacy alias to AJS.dim.$shim
                AJS.dim.shim = AJS.dim.$shim;
            }

            
            AJS.dim.cachedOverflow = $overflowEl.css("overflow");
            $overflowEl.css("overflow", "hidden");
        }

        return AJS.dim.$dim;
    };

    /**
     * Removes semitransparent DIV
     * @see AJS.dim
     */
    AJS.undim = function() {
        if (AJS.dim.$dim) {
            AJS.dim.$dim.remove();
            AJS.dim.$dim = null;

            if (AJS.dim.$shim) {
                AJS.dim.$shim.remove();
                AJS.dim.$shim = null;
            }

            $overflowEl && $overflowEl.css("overflow",  AJS.dim.cachedOverflow);

            // Safari bug workaround
            if ($.browser.safari) {
                var top = $(window).scrollTop();
                $(window).scrollTop(10 + 5 * (top == 10)).scrollTop(top);
            }
        }
    };

}(AJS.$));