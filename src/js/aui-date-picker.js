(function ($) {

    'use strict';

    var datePickerCounter = 0;

    AJS.DatePicker = function (field, options) {

        var datePicker, initPolyfill, $field, datePickerUUID;

        datePicker = {};

        datePickerUUID = datePickerCounter++;

        // ---------------------------------------------------------------------
        // fix up arguments ----------------------------------------------------
        // ---------------------------------------------------------------------

        $field = $(field);
        $field.attr("data-aui-dp-uuid", datePickerUUID);
        options = $.extend(undefined, AJS.DatePicker.prototype.defaultOptions, options);

        // ---------------------------------------------------------------------
        // expose arguments with getters ---------------------------------------
        // ---------------------------------------------------------------------

        datePicker.getField = function () {
            return $field;
        };

        datePicker.getOptions = function () {
            return options;
        };

        // ---------------------------------------------------------------------
        // exposed methods -----------------------------------------------------
        // ---------------------------------------------------------------------

        initPolyfill = function () {

            var calendar, handleDatePickerFocus, handleFieldBlur, handleFieldFocus,
                    handleFieldUpdate, initCalendar, isForcingHide, isSuppressingShow,
                    isTrackingDatePickerFocus, popup, popupContents;

            // -----------------------------------------------------------------
            // expose methods for controlling the popup ------------------------
            // -----------------------------------------------------------------

            datePicker.hide = function () {
                isForcingHide = true;
                popup.hide();
                isForcingHide = false;
            };

            datePicker.show = function () {
                popup.show();
            };

            // -----------------------------------------------------------------
            // initialise the calendar -----------------------------------------
            // -----------------------------------------------------------------

            initCalendar = function () {

                popupContents.off();
                calendar = $('<div/>');
                calendar.attr("data-aui-dp-popup-uuid", datePickerUUID);
                popupContents.append(calendar);

                var config = {
                    'dateFormat': $.datepicker.W3C, // same as $.datepicker.ISO_8601
                    'defaultDate': $field.val(),
                    'maxDate': $field.attr('max'),
                    'minDate': $field.attr('min'),
                    'nextText': '>',
                    'onSelect': function (dateText, inst) {
                        $field.val(dateText);
                        $field.change();
                        datePicker.hide();
                        isSuppressingShow = true;
                        $field.focus();
                    },
                    'prevText': '<'
                };

                if (!(options.languageCode in AJS.DatePicker.prototype.localisations)) {
                    options.languageCode = '';
                }
                $.extend(config, AJS.DatePicker.prototype.localisations[options.languageCode]);

                if (options.firstDay > -1) {
                    config.firstDay = options.firstDay;
                }

                if (typeof $field.attr('step') !== 'undefined') {
                    AJS.log('WARNING: The AJS date picker polyfill currently does not support the step attribute!');
                }

                calendar.datepicker(config);

                // bind additional field processing events
                $field.on('focusout', handleFieldBlur);
                $field.on('propertychange keyup input paste', handleFieldUpdate);

            };

            // -----------------------------------------------------------------
            // event handler wrappers ------------------------------------------
            // -----------------------------------------------------------------

            handleDatePickerFocus = function (event) {
                var $eventTarget = $(event.target);
                event.preventDefault();
                if (!($eventTarget.closest(popupContents).length || $eventTarget.is($field))) {
                    if ($eventTarget.closest('body').length) {
                        datePicker.hide();
                    }
                }
            };

            handleFieldBlur = function (event) {
                if (!(isTrackingDatePickerFocus)) {
                    // hide the date picker if whatever's got focus isn't inside our popup, but does exist inside the page
                    // MSIE fucks this up by somehow only firing the SECOND time an element is focused, but what can you do?
                    $('body').on('focus blur click mousedown', '*', handleDatePickerFocus);
                    isTrackingDatePickerFocus = true;
                }
            };

            handleFieldFocus = function (event) {
                if (!(isSuppressingShow)) {
                    datePicker.show();
                } else {
                    isSuppressingShow = false;
                }
            };

            handleFieldUpdate = function (event) {
                calendar.datepicker('setDate', $field.val());
                calendar.datepicker('option', {
                    'maxDate': $field.attr('max'),
                    'minDate': $field.attr('min')
                });
            };

            // -----------------------------------------------------------------
            // undo (almost) everything ----------------------------------------
            // -----------------------------------------------------------------

            datePicker.destroyPolyfill = function () {

                // goodbye, cruel world!
                datePicker.hide();

                $field.attr('placeholder', null);

                $field.off('propertychange keyup input paste', handleFieldUpdate);
                $field.off('focus click', handleFieldFocus);
                $field.off('focusout', handleFieldBlur);

                $field.attr('type', 'date');

                if (typeof calendar !== 'undefined') {
                    calendar.datepicker('destroy');
                }

                // TODO: figure out a way to tear down the popup (if necessary)

                delete datePicker.destroyPolyfill;

                delete datePicker.show;
                delete datePicker.hide;

            };

            // -----------------------------------------------------------------
            // polyfill bootstrap ----------------------------------------------
            // -----------------------------------------------------------------

            isForcingHide = false; // used in conjunction with the inline dialog pre-hide arbitrator callback to suppress popup-initiated hide attempts
            isSuppressingShow = false; // used to stop the popover from showing when focus is restored to the field after a date has been selected
            isTrackingDatePickerFocus = false; // used to prevent multiple bindings of handleDatePickerFocus within handleFieldBlur

            popup = AJS.InlineDialog($field, undefined, function (contents, trigger, showPopup) {
                if (typeof calendar === 'undefined') {
                    popupContents = contents;
                    initCalendar();
                }
                showPopup();
            }, {
                'hideCallback': function () {
                    $('body').off('focus blur click mousedown', '*', handleDatePickerFocus);
                    isTrackingDatePickerFocus = false;
                },
                'hideDelay': null,
                'noBind': true,
                'preHideCallback': function () {
                    return isForcingHide;
                },
                'width': 300
            });

            // bind what we need to start off with
            $field.on('focus click', handleFieldFocus); // the click is for fucking opera... Y U NO FIRE FOCUS EVENTS PROPERLY???

            // give users a hint that this is a date field; note that placeholder isn't technically a valid attribute
            // according to the spec...
            $field.attr('placeholder', 'YYYY-MM-DD');

            // override the browser's default date field implementation (if applicable)
            // since IE doesn't support date input fields, we should be fine...
            if (options.overrideBrowserDefault && AJS.DatePicker.prototype.browserSupportsDateField) {
                $field[0].type = 'text';
            }

        };

        datePicker.reset = function () {

            if (typeof datePicker.destroyPolyfill === 'function') {
                datePicker.destroyPolyfill();
            }

            if ((!(AJS.DatePicker.prototype.browserSupportsDateField)) || options.overrideBrowserDefault) {
                initPolyfill();
            }

        };

        // ---------------------------------------------------------------------
        // bootstrap -----------------------------------------------------------
        // ---------------------------------------------------------------------

        datePicker.reset();

        return datePicker;

    };

    // -------------------------------------------------------------------------
    // things that should be common --------------------------------------------
    // -------------------------------------------------------------------------

    AJS.DatePicker.prototype.browserSupportsDateField = ($('<input type="date" />')[0].type === 'date');

    AJS.DatePicker.prototype.defaultOptions = {
        overrideBrowserDefault: false,
        firstDay: -1,
        languageCode: AJS.$('html').attr('lang') || 'en-AU'
    };

    // adapted from the jQuery UI Datepicker widget (v1.8.16), with the following changes:
    //   - dayNamesShort -> dayNamesMin
    //   - unnecessary attributes omitted
    /*
    CODE to extract codes out:

    var langCode, langs, out;
    langs = jQuery.datepicker.regional;
    out = {};

    for (langCode in langs) {
        if (langs.hasOwnProperty(langCode)) {
            out[langCode] = {
                'dayNames': langs[langCode].dayNames,
                'dayNamesMin': langs[langCode].dayNamesShort, // this is deliberate
                'firstDay': langs[langCode].firstDay,
                'isRTL': langs[langCode].isRTL,
                'monthNames': langs[langCode].monthNames,
                'showMonthAfterYear': langs[langCode].showMonthAfterYear,
                'yearSuffix': langs[langCode].yearSuffix
            };
        }
    }

     */
    AJS.DatePicker.prototype.localisations = {
        "": {
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesMin": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "af": {
            "dayNames": ["Sondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrydag", "Saterdag"],
            "dayNamesMin": ["Son", "Maa", "Din", "Woe", "Don", "Vry", "Sat"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Januarie", "Februarie", "Maart", "April", "Mei", "Junie", "Julie", "Augustus", "September", "Oktober", "November", "Desember"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ar-DZ": {
            "dayNames": ["Ø§Ù„Ø£Ø­Ø¯", "Ø§Ù„Ø§Ø«Ù†ÙŠÙ†", "Ø§Ù„Ø«Ù„Ø§Ø«Ø§Ø¡", "Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡", "Ø§Ù„Ø®Ù…ÙŠØ³", "Ø§Ù„Ø¬Ù…Ø¹Ø©", "Ø§Ù„Ø³Ø¨Øª"],
            "dayNamesMin": ["Ø§Ù„Ø£Ø­Ø¯", "Ø§Ù„Ø§Ø«Ù†ÙŠÙ†", "Ø§Ù„Ø«Ù„Ø§Ø«Ø§Ø¡", "Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡", "Ø§Ù„Ø®Ù…ÙŠØ³", "Ø§Ù„Ø¬Ù…Ø¹Ø©", "Ø§Ù„Ø³Ø¨Øª"],
            "firstDay": 6,
            "isRTL": true,
            "monthNames": ["Ø¬Ø§Ù†ÙÙŠ", "ÙÙŠÙØ±ÙŠ", "Ù…Ø§Ø±Ø³", "Ø£ÙØ±ÙŠÙ„", "Ù…Ø§ÙŠ", "Ø¬ÙˆØ§Ù†", "Ø¬ÙˆÙŠÙ„ÙŠØ©", "Ø£ÙˆØª", "Ø³Ø¨ØªÙ…Ø¨Ø±", "Ø£ÙƒØªÙˆØ¨Ø±", "Ù†ÙˆÙÙ…Ø¨Ø±", "Ø¯ÙŠØ³Ù…Ø¨Ø±"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ar": {
            "dayNames": ["Ø§Ù„Ø£Ø­Ø¯", "Ø§Ù„Ø§Ø«Ù†ÙŠÙ†", "Ø§Ù„Ø«Ù„Ø§Ø«Ø§Ø¡", "Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡", "Ø§Ù„Ø®Ù…ÙŠØ³", "Ø§Ù„Ø¬Ù…Ø¹Ø©", "Ø§Ù„Ø³Ø¨Øª"],
            "dayNamesMin": ["Ø§Ù„Ø£Ø­Ø¯", "Ø§Ù„Ø§Ø«Ù†ÙŠÙ†", "Ø§Ù„Ø«Ù„Ø§Ø«Ø§Ø¡", "Ø§Ù„Ø£Ø±Ø¨Ø¹Ø§Ø¡", "Ø§Ù„Ø®Ù…ÙŠØ³", "Ø§Ù„Ø¬Ù…Ø¹Ø©", "Ø§Ù„Ø³Ø¨Øª"],
            "firstDay": 6,
            "isRTL": true,
            "monthNames": ["ÙƒØ§Ù†ÙˆÙ† Ø§Ù„Ø«Ø§Ù†ÙŠ", "Ø´Ø¨Ø§Ø·", "Ø¢Ø°Ø§Ø±", "Ù†ÙŠØ³Ø§Ù†", "Ù…Ø§ÙŠÙˆ", "Ø­Ø²ÙŠØ±Ø§Ù†", "ØªÙ…ÙˆØ²", "Ø¢Ø¨", "Ø£ÙŠÙ„ÙˆÙ„", "ØªØ´Ø±ÙŠÙ† Ø§Ù„Ø£ÙˆÙ„", "ØªØ´Ø±ÙŠÙ† Ø§Ù„Ø«Ø§Ù†ÙŠ", "ÙƒØ§Ù†ÙˆÙ† Ø§Ù„Ø£ÙˆÙ„"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "az": {
            "dayNames": ["Bazar", "Bazar ertÉ™si", "Ã‡É™rÅŸÉ™nbÉ™ axÅŸamÄ±", "Ã‡É™rÅŸÉ™nbÉ™", "CÃ¼mÉ™ axÅŸamÄ±", "CÃ¼mÉ™", "ÅžÉ™nbÉ™"],
            "dayNamesMin": ["B", "Be", "Ã‡a", "Ã‡", "Ca", "C", "Åž"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Yanvar", "Fevral", "Mart", "Aprel", "May", "Ä°yun", "Ä°yul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "bg": {
            "dayNames": ["ÐÐµÐ´ÐµÐ»Ñ", "ÐŸÐ¾Ð½ÐµÐ´ÐµÐ»Ð½Ð¸Ðº", "Ð’Ñ‚Ð¾Ñ€Ð½Ð¸Ðº", "Ð¡Ñ€ÑÐ´Ð°", "Ð§ÐµÑ‚Ð²ÑŠÑ€Ñ‚ÑŠÐº", "ÐŸÐµÑ‚ÑŠÐº", "Ð¡ÑŠÐ±Ð¾Ñ‚Ð°"],
            "dayNamesMin": ["ÐÐµÐ´", "ÐŸÐ¾Ð½", "Ð’Ñ‚Ð¾", "Ð¡Ñ€Ñ", "Ð§ÐµÑ‚", "ÐŸÐµÑ‚", "Ð¡ÑŠÐ±"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Ð¯Ð½ÑƒÐ°Ñ€Ð¸", "Ð¤ÐµÐ²Ñ€ÑƒÐ°Ñ€Ð¸", "ÐœÐ°Ñ€Ñ‚", "ÐÐ¿Ñ€Ð¸Ð»", "ÐœÐ°Ð¹", "Ð®Ð½Ð¸", "Ð®Ð»Ð¸", "ÐÐ²Ð³ÑƒÑÑ‚", "Ð¡ÐµÐ¿Ñ‚ÐµÐ¼Ð²Ñ€Ð¸", "ÐžÐºÑ‚Ð¾Ð¼Ð²Ñ€Ð¸", "ÐÐ¾ÐµÐ¼Ð²Ñ€Ð¸", "Ð”ÐµÐºÐµÐ¼Ð²Ñ€Ð¸"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "bs": {
            "dayNames": ["Nedelja", "Ponedeljak", "Utorak", "Srijeda", "ÄŒetvrtak", "Petak", "Subota"],
            "dayNamesMin": ["Ned", "Pon", "Uto", "Sri", "ÄŒet", "Pet", "Sub"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ca": {
            "dayNames": ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"],
            "dayNamesMin": ["Dug", "Dln", "Dmt", "Dmc", "Djs", "Dvn", "Dsb"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Gener", "Febrer", "Mar&ccedil;", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "cs": {
            "dayNames": ["nedÄ›le", "pondÄ›lÃ­", "ÃºterÃ½", "stÅ™eda", "Ätvrtek", "pÃ¡tek", "sobota"],
            "dayNamesMin": ["ne", "po", "Ãºt", "st", "Ät", "pÃ¡", "so"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["leden", "Ãºnor", "bÅ™ezen", "duben", "kvÄ›ten", "Äerven", "Äervenec", "srpen", "zÃ¡Å™Ã­", "Å™Ã­jen", "listopad", "prosinec"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "cy-GB": {
            "dayNames": ["Dydd Sul", "Dydd Llun", "Dydd Mawrth", "Dydd Mercher", "Dydd Iau", "Dydd Gwener", "Dydd Sadwrn"],
            "dayNamesMin": ["Sul", "Llu", "Maw", "Mer", "Iau", "Gwe", "Sad"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Ionawr", "Chwefror", "Mawrth", "Ebrill", "Mai", "Mehefin", "Gorffennaf", "Awst", "Medi", "Hydref", "Tachwedd", "Rhagfyr"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "da": {
            "dayNames": ["SÃ¸ndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "LÃ¸rdag"],
            "dayNamesMin": ["SÃ¸n", "Man", "Tir", "Ons", "Tor", "Fre", "LÃ¸r"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "de": {
            "dayNames": ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
            "dayNamesMin": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Januar", "Februar", "MÃ¤rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "el": {
            "dayNames": ["ÎšÏ…ÏÎ¹Î±ÎºÎ®", "Î”ÎµÏ…Ï„Î­ÏÎ±", "Î¤ÏÎ¯Ï„Î·", "Î¤ÎµÏ„Î¬ÏÏ„Î·", "Î Î­Î¼Ï€Ï„Î·", "Î Î±ÏÎ±ÏƒÎºÎµÏ…Î®", "Î£Î¬Î²Î²Î±Ï„Î¿"],
            "dayNamesMin": ["ÎšÏ…Ï", "Î”ÎµÏ…", "Î¤ÏÎ¹", "Î¤ÎµÏ„", "Î ÎµÎ¼", "Î Î±Ï", "Î£Î±Î²"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Î™Î±Î½Î¿Ï…Î¬ÏÎ¹Î¿Ï‚", "Î¦ÎµÎ²ÏÎ¿Ï…Î¬ÏÎ¹Î¿Ï‚", "ÎœÎ¬ÏÏ„Î¹Î¿Ï‚", "Î‘Ï€ÏÎ¯Î»Î¹Î¿Ï‚", "ÎœÎ¬Î¹Î¿Ï‚", "Î™Î¿ÏÎ½Î¹Î¿Ï‚", "Î™Î¿ÏÎ»Î¹Î¿Ï‚", "Î‘ÏÎ³Î¿Ï…ÏƒÏ„Î¿Ï‚", "Î£ÎµÏ€Ï„Î­Î¼Î²ÏÎ¹Î¿Ï‚", "ÎŸÎºÏ„ÏŽÎ²ÏÎ¹Î¿Ï‚", "ÎÎ¿Î­Î¼Î²ÏÎ¹Î¿Ï‚", "Î”ÎµÎºÎ­Î¼Î²ÏÎ¹Î¿Ï‚"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "en-AU": {
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesMin": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "en-GB": {
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesMin": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "en-NZ": {
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesMin": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "eo": {
            "dayNames": ["DimanÄ‰o", "Lundo", "Mardo", "Merkredo", "Ä´aÅ­do", "Vendredo", "Sabato"],
            "dayNamesMin": ["Dim", "Lun", "Mar", "Mer", "Ä´aÅ­", "Ven", "Sab"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["Januaro", "Februaro", "Marto", "Aprilo", "Majo", "Junio", "Julio", "AÅ­gusto", "Septembro", "Oktobro", "Novembro", "Decembro"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "es": {
            "dayNames": ["Domingo", "Lunes", "Martes", "Mi&eacute;rcoles", "Jueves", "Viernes", "S&aacute;bado"],
            "dayNamesMin": ["Dom", "Lun", "Mar", "Mi&eacute;", "Juv", "Vie", "S&aacute;b"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "et": {
            "dayNames": ["PÃ¼hapÃ¤ev", "EsmaspÃ¤ev", "TeisipÃ¤ev", "KolmapÃ¤ev", "NeljapÃ¤ev", "Reede", "LaupÃ¤ev"],
            "dayNamesMin": ["PÃ¼hap", "Esmasp", "Teisip", "Kolmap", "Neljap", "Reede", "Laup"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Jaanuar", "Veebruar", "MÃ¤rts", "Aprill", "Mai", "Juuni", "Juuli", "August", "September", "Oktoober", "November", "Detsember"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "eu": {
            "dayNames": ["Igandea", "Astelehena", "Asteartea", "Asteazkena", "Osteguna", "Ostirala", "Larunbata"],
            "dayNamesMin": ["Iga", "Ast", "Ast", "Ast", "Ost", "Ost", "Lar"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "fa": {
            "dayNames": ["ÙŠÚ©Ø´Ù†Ø¨Ù‡", "Ø¯ÙˆØ´Ù†Ø¨Ù‡", "Ø³Ù‡â€ŒØ´Ù†Ø¨Ù‡", "Ú†Ù‡Ø§Ø±Ø´Ù†Ø¨Ù‡", "Ù¾Ù†Ø¬Ø´Ù†Ø¨Ù‡", "Ø¬Ù…Ø¹Ù‡", "Ø´Ù†Ø¨Ù‡"],
            "dayNamesMin": ["ÙŠ", "Ø¯", "Ø³", "Ú†", "Ù¾", "Ø¬", "Ø´"],
            "firstDay": 6,
            "isRTL": true,
            "monthNames": ["ÙØ±ÙˆØ±Ø¯ÙŠÙ†", "Ø§Ø±Ø¯ÙŠØ¨Ù‡Ø´Øª", "Ø®Ø±Ø¯Ø§Ø¯", "ØªÙŠØ±", "Ù…Ø±Ø¯Ø§Ø¯", "Ø´Ù‡Ø±ÙŠÙˆØ±", "Ù…Ù‡Ø±", "Ø¢Ø¨Ø§Ù†", "Ø¢Ø°Ø±", "Ø¯ÙŠ", "Ø¨Ù‡Ù…Ù†", "Ø§Ø³ÙÙ†Ø¯"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "fi": {
            "dayNames": ["Sunnuntai", "Maanantai", "Tiistai", "Keskiviikko", "Torstai", "Perjantai", "Lauantai"],
            "dayNamesMin": ["Su", "Ma", "Ti", "Ke", "To", "Pe", "Su"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "Kes&auml;kuu", "Hein&auml;kuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "fo": {
            "dayNames": ["Sunnudagur", "MÃ¡nadagur", "TÃ½sdagur", "Mikudagur", "HÃ³sdagur", "FrÃ­ggjadagur", "Leyardagur"],
            "dayNamesMin": ["Sun", "MÃ¡n", "TÃ½s", "Mik", "HÃ³s", "FrÃ­", "Ley"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["Januar", "Februar", "Mars", "AprÃ­l", "Mei", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "fr-CH": {
            "dayNames": ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
            "dayNamesMin": ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Janvier", "FÃ©vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "AoÃ»t", "Septembre", "Octobre", "Novembre", "DÃ©cembre"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "fr": {
            "dayNames": ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
            "dayNamesMin": ["Dim.", "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam."],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Janvier", "FÃ©vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "AoÃ»t", "Septembre", "Octobre", "Novembre", "DÃ©cembre"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "gl": {
            "dayNames": ["Domingo", "Luns", "Martes", "M&eacute;rcores", "Xoves", "Venres", "S&aacute;bado"],
            "dayNamesMin": ["Dom", "Lun", "Mar", "M&eacute;r", "Xov", "Ven", "S&aacute;b"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Xaneiro", "Febreiro", "Marzo", "Abril", "Maio", "XuÃ±o", "Xullo", "Agosto", "Setembro", "Outubro", "Novembro", "Decembro"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "he": {
            "dayNames": ["×¨××©×•×Ÿ", "×©× ×™", "×©×œ×™×©×™", "×¨×‘×™×¢×™", "×—×ž×™×©×™", "×©×™×©×™", "×©×‘×ª"],
            "dayNamesMin": ["×'", "×‘'", "×’'", "×“'", "×”'", "×•'", "×©×‘×ª"],
            "firstDay": 0,
            "isRTL": true,
            "monthNames": ["×™× ×•××¨", "×¤×‘×¨×•××¨", "×ž×¨×¥", "××¤×¨×™×œ", "×ž××™", "×™×•× ×™", "×™×•×œ×™", "××•×’×•×¡×˜", "×¡×¤×˜×ž×‘×¨", "××•×§×˜×•×‘×¨", "× ×•×‘×ž×‘×¨", "×“×¦×ž×‘×¨"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "hr": {
            "dayNames": ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "ÄŒetvrtak", "Petak", "Subota"],
            "dayNamesMin": ["Ned", "Pon", "Uto", "Sri", "ÄŒet", "Pet", "Sub"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["SijeÄanj", "VeljaÄa", "OÅ¾ujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "hu": {
            "dayNames": ["VasÃ¡rnap", "HÃ©tfÅ‘", "Kedd", "Szerda", "CsÃ¼tÃ¶rtÃ¶k", "PÃ©ntek", "Szombat"],
            "dayNamesMin": ["Vas", "HÃ©t", "Ked", "Sze", "CsÃ¼", "PÃ©n", "Szo"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["JanuÃ¡r", "FebruÃ¡r", "MÃ¡rcius", "Ãprilis", "MÃ¡jus", "JÃºnius", "JÃºlius", "Augusztus", "Szeptember", "OktÃ³ber", "November", "December"],
            "showMonthAfterYear": true,
            "yearSuffix": ""
        },
        "hy": {
            "dayNames": ["Õ¯Õ«Ö€Õ¡Õ¯Õ«", "Õ¥Õ¯Õ¸Ö‚Õ·Õ¡Õ¢Õ©Õ«", "Õ¥Ö€Õ¥Ö„Õ·Õ¡Õ¢Õ©Õ«", "Õ¹Õ¸Ö€Õ¥Ö„Õ·Õ¡Õ¢Õ©Õ«", "Õ°Õ«Õ¶Õ£Õ·Õ¡Õ¢Õ©Õ«", "Õ¸Ö‚Ö€Õ¢Õ¡Õ©", "Õ·Õ¡Õ¢Õ¡Õ©"],
            "dayNamesMin": ["Õ¯Õ«Ö€", "Õ¥Ö€Õ¯", "Õ¥Ö€Ö„", "Õ¹Ö€Ö„", "Õ°Õ¶Õ£", "Õ¸Ö‚Ö€Õ¢", "Õ·Õ¢Õ©"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Õ€Õ¸Ö‚Õ¶Õ¾Õ¡Ö€", "Õ“Õ¥Õ¿Ö€Õ¾Õ¡Ö€", "Õ„Õ¡Ö€Õ¿", "Ô±ÕºÖ€Õ«Õ¬", "Õ„Õ¡ÕµÕ«Õ½", "Õ€Õ¸Ö‚Õ¶Õ«Õ½", "Õ€Õ¸Ö‚Õ¬Õ«Õ½", "Õ•Õ£Õ¸Õ½Õ¿Õ¸Õ½", "ÕÕ¥ÕºÕ¿Õ¥Õ´Õ¢Õ¥Ö€", "Õ€Õ¸Õ¯Õ¿Õ¥Õ´Õ¢Õ¥Ö€", "Õ†Õ¸ÕµÕ¥Õ´Õ¢Õ¥Ö€", "Ô´Õ¥Õ¯Õ¿Õ¥Õ´Õ¢Õ¥Ö€"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "id": {
            "dayNames": ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
            "dayNamesMin": ["Min", "Sen", "Sel", "Rab", "kam", "Jum", "Sab"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "is": {
            "dayNames": ["Sunnudagur", "M&aacute;nudagur", "&THORN;ri&eth;judagur", "Mi&eth;vikudagur", "Fimmtudagur", "F&ouml;studagur", "Laugardagur"],
            "dayNamesMin": ["Sun", "M&aacute;n", "&THORN;ri", "Mi&eth;", "Fim", "F&ouml;s", "Lau"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["Jan&uacute;ar", "Febr&uacute;ar", "Mars", "Apr&iacute;l", "Ma&iacute", "J&uacute;n&iacute;", "J&uacute;l&iacute;", "&Aacute;g&uacute;st", "September", "Okt&oacute;ber", "N&oacute;vember", "Desember"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "it": {
            "dayNames": ["Domenica", "Luned&#236", "Marted&#236", "Mercoled&#236", "Gioved&#236", "Venerd&#236", "Sabato"],
            "dayNamesMin": ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ja": {
            "dayNames": ["æ—¥æ›œæ—¥", "æœˆæ›œæ—¥", "ç«æ›œæ—¥", "æ°´æ›œæ—¥", "æœ¨æ›œæ—¥", "é‡‘æ›œæ—¥", "åœŸæ›œæ—¥"],
            "dayNamesMin": ["æ—¥", "æœˆ", "ç«", "æ°´", "æœ¨", "é‡‘", "åœŸ"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["1æœˆ", "2æœˆ", "3æœˆ", "4æœˆ", "5æœˆ", "6æœˆ", "7æœˆ", "8æœˆ", "9æœˆ", "10æœˆ", "11æœˆ", "12æœˆ"],
            "showMonthAfterYear": true,
            "yearSuffix": "å¹´"
        },
        "kk": {
            "dayNames": ["Ð–ÐµÐºÑÐµÐ½Ð±Ñ–", "Ð”Ò¯Ð¹ÑÐµÐ½Ð±Ñ–", "Ð¡ÐµÐ¹ÑÐµÐ½Ð±Ñ–", "Ð¡Ó™Ñ€ÑÐµÐ½Ð±Ñ–", "Ð‘ÐµÐ¹ÑÐµÐ½Ð±Ñ–", "Ð–Ò±Ð¼Ð°", "Ð¡ÐµÐ½Ð±Ñ–"],
            "dayNamesMin": ["Ð¶ÐºÑ", "Ð´ÑÐ½", "ÑÑÐ½", "ÑÑ€Ñ", "Ð±ÑÐ½", "Ð¶Ð¼Ð°", "ÑÐ½Ð±"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["ÒšÐ°Ò£Ñ‚Ð°Ñ€", "ÐÒ›Ð¿Ð°Ð½", "ÐÐ°ÑƒÑ€Ñ‹Ð·", "Ð¡Ó™ÑƒÑ–Ñ€", "ÐœÐ°Ð¼Ñ‹Ñ€", "ÐœÐ°ÑƒÑÑ‹Ð¼", "Ð¨Ñ–Ð»Ð´Ðµ", "Ð¢Ð°Ð¼Ñ‹Ð·", "ÒšÑ‹Ñ€ÐºÒ¯Ð¹ÐµÐº", "ÒšÐ°Ð·Ð°Ð½", "ÒšÐ°Ñ€Ð°ÑˆÐ°", "Ð–ÐµÐ»Ñ‚Ð¾Ò›ÑÐ°Ð½"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ko": {
            "dayNames": ["ì¼", "ì›”", "í™”", "ìˆ˜", "ëª©", "ê¸ˆ", "í† "],
            "dayNamesMin": ["ì¼", "ì›”", "í™”", "ìˆ˜", "ëª©", "ê¸ˆ", "í† "],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["1ì›”(JAN)", "2ì›”(FEB)", "3ì›”(MAR)", "4ì›”(APR)", "5ì›”(MAY)", "6ì›”(JUN)", "7ì›”(JUL)", "8ì›”(AUG)", "9ì›”(SEP)", "10ì›”(OCT)", "11ì›”(NOV)", "12ì›”(DEC)"],
            "showMonthAfterYear": false,
            "yearSuffix": "ë…„"
        },
        "lb": {
            "dayNames": ["Sonndeg", "MÃ©indeg", "DÃ«nschdeg", "MÃ«ttwoch", "Donneschdeg", "Freideg", "Samschdeg"],
            "dayNamesMin": ["Son", "MÃ©i", "DÃ«n", "MÃ«t", "Don", "Fre", "Sam"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Januar", "Februar", "MÃ¤erz", "AbrÃ«ll", "Mee", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "lt": {
            "dayNames": ["sekmadienis", "pirmadienis", "antradienis", "treÄiadienis", "ketvirtadienis", "penktadienis", "Å¡eÅ¡tadienis"],
            "dayNamesMin": ["sek", "pir", "ant", "tre", "ket", "pen", "Å¡eÅ¡"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Sausis", "Vasaris", "Kovas", "Balandis", "GeguÅ¾Ä—", "BirÅ¾elis", "Liepa", "RugpjÅ«tis", "RugsÄ—jis", "Spalis", "Lapkritis", "Gruodis"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "lv": {
            "dayNames": ["svÄ“tdiena", "pirmdiena", "otrdiena", "treÅ¡diena", "ceturtdiena", "piektdiena", "sestdiena"],
            "dayNamesMin": ["svt", "prm", "otr", "tre", "ctr", "pkt", "sst"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["JanvÄris", "FebruÄris", "Marts", "AprÄ«lis", "Maijs", "JÅ«nijs", "JÅ«lijs", "Augusts", "Septembris", "Oktobris", "Novembris", "Decembris"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "mk": {
            "dayNames": ["ÐÐµÐ´ÐµÐ»Ð°", "ÐŸÐ¾Ð½ÐµÐ´ÐµÐ»Ð½Ð¸Ðº", "Ð’Ñ‚Ð¾Ñ€Ð½Ð¸Ðº", "Ð¡Ñ€ÐµÐ´Ð°", "Ð§ÐµÑ‚Ð²Ñ€Ñ‚Ð¾Ðº", "ÐŸÐµÑ‚Ð¾Ðº", "Ð¡Ð°Ð±Ð¾Ñ‚Ð°"],
            "dayNamesMin": ["ÐÐµÐ´", "ÐŸÐ¾Ð½", "Ð’Ñ‚Ð¾", "Ð¡Ñ€Ðµ", "Ð§ÐµÑ‚", "ÐŸÐµÑ‚", "Ð¡Ð°Ð±"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["ÐˆÐ°Ð½ÑƒÐ°Ñ€Ð¸", "Ð¤ÐµÐ±Ñ€ÑƒÐ°Ñ€Ð¸", "ÐœÐ°Ñ€Ñ‚", "ÐÐ¿Ñ€Ð¸Ð»", "ÐœÐ°Ñ˜", "ÐˆÑƒÐ½Ð¸", "ÐˆÑƒÐ»Ð¸", "ÐÐ²Ð³ÑƒÑÑ‚", "Ð¡ÐµÐ¿Ñ‚ÐµÐ¼Ð²Ñ€Ð¸", "ÐžÐºÑ‚Ð¾Ð¼Ð²Ñ€Ð¸", "ÐÐ¾ÐµÐ¼Ð²Ñ€Ð¸", "Ð”ÐµÐºÐµÐ¼Ð²Ñ€Ð¸"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ml": {
            "dayNames": ["à´žà´¾à´¯à´°àµâ€", "à´¤à´¿à´™àµà´•à´³àµâ€", "à´šàµŠà´µàµà´µ", "à´¬àµà´§à´¨àµâ€", "à´µàµà´¯à´¾à´´à´‚", "à´µàµ†à´³àµà´³à´¿", "à´¶à´¨à´¿"],
            "dayNamesMin": ["à´žà´¾à´¯", "à´¤à´¿à´™àµà´•", "à´šàµŠà´µàµà´µ", "à´¬àµà´§", "à´µàµà´¯à´¾à´´à´‚", "à´µàµ†à´³àµà´³à´¿", "à´¶à´¨à´¿"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["à´œà´¨àµà´µà´°à´¿", "à´«àµ†à´¬àµà´°àµà´µà´°à´¿", "à´®à´¾à´°àµâ€à´šàµà´šàµ", "à´à´ªàµà´°à´¿à´²àµâ€", "à´®àµ‡à´¯àµ", "à´œàµ‚à´£àµâ€", "à´œàµ‚à´²àµˆ", "à´†à´—à´¸àµà´±àµà´±àµ", "à´¸àµ†à´ªàµà´±àµà´±à´‚à´¬à´°àµâ€", "à´’à´•àµà´Ÿàµ‹à´¬à´°àµâ€", "à´¨à´µà´‚à´¬à´°àµâ€", "à´¡à´¿à´¸à´‚à´¬à´°àµâ€"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ms": {
            "dayNames": ["Ahad", "Isnin", "Selasa", "Rabu", "Khamis", "Jumaat", "Sabtu"],
            "dayNamesMin": ["Aha", "Isn", "Sel", "Rab", "kha", "Jum", "Sab"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["Januari", "Februari", "Mac", "April", "Mei", "Jun", "Julai", "Ogos", "September", "Oktober", "November", "Disember"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "nl-BE": {
            "dayNames": ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"],
            "dayNamesMin": ["zon", "maa", "din", "woe", "don", "vri", "zat"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "nl": {
            "dayNames": ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"],
            "dayNamesMin": ["zon", "maa", "din", "woe", "don", "vri", "zat"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "no": {
            "dayNames": ["sÃ¸ndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lÃ¸rdag"],
            "dayNamesMin": ["sÃ¸n", "man", "tir", "ons", "tor", "fre", "lÃ¸r"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "pl": {
            "dayNames": ["Niedziela", "PoniedziaÅ‚ek", "Wtorek", "Åšroda", "Czwartek", "PiÄ…tek", "Sobota"],
            "dayNamesMin": ["Nie", "Pn", "Wt", "Åšr", "Czw", "Pt", "So"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["StyczeÅ„", "Luty", "Marzec", "KwiecieÅ„", "Maj", "Czerwiec", "Lipiec", "SierpieÅ„", "WrzesieÅ„", "PaÅºdziernik", "Listopad", "GrudzieÅ„"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "pt-BR": {
            "dayNames": ["Domingo", "Segunda-feira", "Ter&ccedil;a-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "S&aacute;bado"],
            "dayNamesMin": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&aacute;b"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "pt": {
            "dayNames": ["Domingo", "Segunda-feira", "Ter&ccedil;a-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "S&aacute;bado"],
            "dayNamesMin": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&aacute;b"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "rm": {
            "dayNames": ["Dumengia", "Glindesdi", "Mardi", "Mesemna", "Gievgia", "Venderdi", "Sonda"],
            "dayNamesMin": ["Dum", "Gli", "Mar", "Mes", "Gie", "Ven", "Som"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Schaner", "Favrer", "Mars", "Avrigl", "Matg", "Zercladur", "Fanadur", "Avust", "Settember", "October", "November", "December"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ro": {
            "dayNames": ["DuminicÄƒ", "Luni", "MarÅ£i", "Miercuri", "Joi", "Vineri", "SÃ¢mbÄƒtÄƒ"],
            "dayNamesMin": ["Dum", "Lun", "Mar", "Mie", "Joi", "Vin", "SÃ¢m"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ru": {
            "dayNames": ["Ð²Ð¾ÑÐºÑ€ÐµÑÐµÐ½ÑŒÐµ", "Ð¿Ð¾Ð½ÐµÐ´ÐµÐ»ÑŒÐ½Ð¸Ðº", "Ð²Ñ‚Ð¾Ñ€Ð½Ð¸Ðº", "ÑÑ€ÐµÐ´Ð°", "Ñ‡ÐµÑ‚Ð²ÐµÑ€Ð³", "Ð¿ÑÑ‚Ð½Ð¸Ñ†Ð°", "ÑÑƒÐ±Ð±Ð¾Ñ‚Ð°"],
            "dayNamesMin": ["Ð²ÑÐº", "Ð¿Ð½Ð´", "Ð²Ñ‚Ñ€", "ÑÑ€Ð´", "Ñ‡Ñ‚Ð²", "Ð¿Ñ‚Ð½", "ÑÐ±Ñ‚"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Ð¯Ð½Ð²Ð°Ñ€ÑŒ", "Ð¤ÐµÐ²Ñ€Ð°Ð»ÑŒ", "ÐœÐ°Ñ€Ñ‚", "ÐÐ¿Ñ€ÐµÐ»ÑŒ", "ÐœÐ°Ð¹", "Ð˜ÑŽÐ½ÑŒ", "Ð˜ÑŽÐ»ÑŒ", "ÐÐ²Ð³ÑƒÑÑ‚", "Ð¡ÐµÐ½Ñ‚ÑÐ±Ñ€ÑŒ", "ÐžÐºÑ‚ÑÐ±Ñ€ÑŒ", "ÐÐ¾ÑÐ±Ñ€ÑŒ", "Ð”ÐµÐºÐ°Ð±Ñ€ÑŒ"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "sk": {
            "dayNames": ["NedeÄ¾a", "Pondelok", "Utorok", "Streda", "Å tvrtok", "Piatok", "Sobota"],
            "dayNamesMin": ["Ned", "Pon", "Uto", "Str", "Å tv", "Pia", "Sob"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["JanuÃ¡r", "FebruÃ¡r", "Marec", "AprÃ­l", "MÃ¡j", "JÃºn", "JÃºl", "August", "September", "OktÃ³ber", "November", "December"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "sl": {
            "dayNames": ["Nedelja", "Ponedeljek", "Torek", "Sreda", "&#x10C;etrtek", "Petek", "Sobota"],
            "dayNamesMin": ["Ned", "Pon", "Tor", "Sre", "&#x10C;et", "Pet", "Sob"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "sq": {
            "dayNames": ["E Diel", "E HÃ«nÃ«", "E MartÃ«", "E MÃ«rkurÃ«", "E Enjte", "E Premte", "E Shtune"],
            "dayNamesMin": ["Di", "HÃ«", "Ma", "MÃ«", "En", "Pr", "Sh"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Janar", "Shkurt", "Mars", "Prill", "Maj", "Qershor", "Korrik", "Gusht", "Shtator", "Tetor", "NÃ«ntor", "Dhjetor"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "sr-SR": {
            "dayNames": ["Nedelja", "Ponedeljak", "Utorak", "Sreda", "ÄŒetvrtak", "Petak", "Subota"],
            "dayNamesMin": ["Ned", "Pon", "Uto", "Sre", "ÄŒet", "Pet", "Sub"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "sr": {
            "dayNames": ["ÐÐµÐ´ÐµÑ™Ð°", "ÐŸÐ¾Ð½ÐµÐ´ÐµÑ™Ð°Ðº", "Ð£Ñ‚Ð¾Ñ€Ð°Ðº", "Ð¡Ñ€ÐµÐ´Ð°", "Ð§ÐµÑ‚Ð²Ñ€Ñ‚Ð°Ðº", "ÐŸÐµÑ‚Ð°Ðº", "Ð¡ÑƒÐ±Ð¾Ñ‚Ð°"],
            "dayNamesMin": ["ÐÐµÐ´", "ÐŸÐ¾Ð½", "Ð£Ñ‚Ð¾", "Ð¡Ñ€Ðµ", "Ð§ÐµÑ‚", "ÐŸÐµÑ‚", "Ð¡ÑƒÐ±"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["ÐˆÐ°Ð½ÑƒÐ°Ñ€", "Ð¤ÐµÐ±Ñ€ÑƒÐ°Ñ€", "ÐœÐ°Ñ€Ñ‚", "ÐÐ¿Ñ€Ð¸Ð»", "ÐœÐ°Ñ˜", "ÐˆÑƒÐ½", "ÐˆÑƒÐ»", "ÐÐ²Ð³ÑƒÑÑ‚", "Ð¡ÐµÐ¿Ñ‚ÐµÐ¼Ð±Ð°Ñ€", "ÐžÐºÑ‚Ð¾Ð±Ð°Ñ€", "ÐÐ¾Ð²ÐµÐ¼Ð±Ð°Ñ€", "Ð”ÐµÑ†ÐµÐ¼Ð±Ð°Ñ€"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "sv": {
            "dayNames": ["SÃ¶ndag", "MÃ¥ndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "LÃ¶rdag"],
            "dayNamesMin": ["SÃ¶n", "MÃ¥n", "Tis", "Ons", "Tor", "Fre", "LÃ¶r"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "ta": {
            "dayNames": ["à®žà®¾à®¯à®¿à®±à¯à®±à¯à®•à¯à®•à®¿à®´à®®à¯ˆ", "à®¤à®¿à®™à¯à®•à®Ÿà¯à®•à®¿à®´à®®à¯ˆ", "à®šà¯†à®µà¯à®µà®¾à®¯à¯à®•à¯à®•à®¿à®´à®®à¯ˆ", "à®ªà¯à®¤à®©à¯à®•à®¿à®´à®®à¯ˆ", "à®µà®¿à®¯à®¾à®´à®•à¯à®•à®¿à®´à®®à¯ˆ", "à®µà¯†à®³à¯à®³à®¿à®•à¯à®•à®¿à®´à®®à¯ˆ", "à®šà®©à®¿à®•à¯à®•à®¿à®´à®®à¯ˆ"],
            "dayNamesMin": ["à®žà®¾à®¯à®¿à®±à¯", "à®¤à®¿à®™à¯à®•à®³à¯", "à®šà¯†à®µà¯à®µà®¾à®¯à¯", "à®ªà¯à®¤à®©à¯", "à®µà®¿à®¯à®¾à®´à®©à¯", "à®µà¯†à®³à¯à®³à®¿", "à®šà®©à®¿"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["à®¤à¯ˆ", "à®®à®¾à®šà®¿", "à®ªà®™à¯à®•à¯à®©à®¿", "à®šà®¿à®¤à¯à®¤à®¿à®°à¯ˆ", "à®µà¯ˆà®•à®¾à®šà®¿", "à®†à®©à®¿", "à®†à®Ÿà®¿", "à®†à®µà®£à®¿", "à®ªà¯à®°à®Ÿà¯à®Ÿà®¾à®šà®¿", "à®à®ªà¯à®ªà®šà®¿", "à®•à®¾à®°à¯à®¤à¯à®¤à®¿à®•à¯ˆ", "à®®à®¾à®°à¯à®•à®´à®¿"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "th": {
            "dayNames": ["à¸­à¸²à¸—à¸´à¸•à¸¢à¹Œ", "à¸ˆà¸±à¸™à¸—à¸£à¹Œ", "à¸­à¸±à¸‡à¸„à¸²à¸£", "à¸žà¸¸à¸˜", "à¸žà¸¤à¸«à¸±à¸ªà¸šà¸”à¸µ", "à¸¨à¸¸à¸à¸£à¹Œ", "à¹€à¸ªà¸²à¸£à¹Œ"],
            "dayNamesMin": ["à¸­à¸².", "à¸ˆ.", "à¸­.", "à¸ž.", "à¸žà¸¤.", "à¸¨.", "à¸ª."],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["à¸¡à¸à¸£à¸²à¸„à¸¡", "à¸à¸¸à¸¡à¸ à¸²à¸žà¸±à¸™à¸˜à¹Œ", "à¸¡à¸µà¸™à¸²à¸„à¸¡", "à¹€à¸¡à¸©à¸²à¸¢à¸™", "à¸žà¸¤à¸©à¸ à¸²à¸„à¸¡", "à¸¡à¸´à¸–à¸¸à¸™à¸²à¸¢à¸™", "à¸à¸£à¸à¸Žà¸²à¸„à¸¡", "à¸ªà¸´à¸‡à¸«à¸²à¸„à¸¡", "à¸à¸±à¸™à¸¢à¸²à¸¢à¸™", "à¸•à¸¸à¸¥à¸²à¸„à¸¡", "à¸žà¸¤à¸¨à¸ˆà¸´à¸à¸²à¸¢à¸™", "à¸˜à¸±à¸™à¸§à¸²à¸„à¸¡"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "tj": {
            "dayNames": ["ÑÐºÑˆÐ°Ð½Ð±Ðµ", "Ð´ÑƒÑˆÐ°Ð½Ð±Ðµ", "ÑÐµÑˆÐ°Ð½Ð±Ðµ", "Ñ‡Ð¾Ñ€ÑˆÐ°Ð½Ð±Ðµ", "Ð¿Ð°Ð½Ò·ÑˆÐ°Ð½Ð±Ðµ", "Ò·ÑƒÐ¼ÑŠÐ°", "ÑˆÐ°Ð½Ð±Ðµ"],
            "dayNamesMin": ["ÑÐºÑˆ", "Ð´ÑƒÑˆ", "ÑÐµÑˆ", "Ñ‡Ð¾Ñ€", "Ð¿Ð°Ð½", "Ò·ÑƒÐ¼", "ÑˆÐ°Ð½"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Ð¯Ð½Ð²Ð°Ñ€", "Ð¤ÐµÐ²Ñ€Ð°Ð»", "ÐœÐ°Ñ€Ñ‚", "ÐÐ¿Ñ€ÐµÐ»", "ÐœÐ°Ð¹", "Ð˜ÑŽÐ½", "Ð˜ÑŽÐ»", "ÐÐ²Ð³ÑƒÑÑ‚", "Ð¡ÐµÐ½Ñ‚ÑÐ±Ñ€", "ÐžÐºÑ‚ÑÐ±Ñ€", "ÐÐ¾ÑÐ±Ñ€", "Ð”ÐµÐºÐ°Ð±Ñ€"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "tr": {
            "dayNames": ["Pazar", "Pazartesi", "SalÄ±", "Ã‡arÅŸamba", "PerÅŸembe", "Cuma", "Cumartesi"],
            "dayNamesMin": ["Pz", "Pt", "Sa", "Ã‡a", "Pe", "Cu", "Ct"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Ocak", "Åžubat", "Mart", "Nisan", "MayÄ±s", "Haziran", "Temmuz", "AÄŸustos", "EylÃ¼l", "Ekim", "KasÄ±m", "AralÄ±k"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "uk": {
            "dayNames": ["Ð½ÐµÐ´Ñ–Ð»Ñ", "Ð¿Ð¾Ð½ÐµÐ´Ñ–Ð»Ð¾Ðº", "Ð²Ñ–Ð²Ñ‚Ð¾Ñ€Ð¾Ðº", "ÑÐµÑ€ÐµÐ´Ð°", "Ñ‡ÐµÑ‚Ð²ÐµÑ€", "Ð¿â€™ÑÑ‚Ð½Ð¸Ñ†Ñ", "ÑÑƒÐ±Ð¾Ñ‚Ð°"],
            "dayNamesMin": ["Ð½ÐµÐ´", "Ð¿Ð½Ð´", "Ð²Ñ–Ð²", "ÑÑ€Ð´", "Ñ‡Ñ‚Ð²", "Ð¿Ñ‚Ð½", "ÑÐ±Ñ‚"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["Ð¡Ñ–Ñ‡ÐµÐ½ÑŒ", "Ð›ÑŽÑ‚Ð¸Ð¹", "Ð‘ÐµÑ€ÐµÐ·ÐµÐ½ÑŒ", "ÐšÐ²Ñ–Ñ‚ÐµÐ½ÑŒ", "Ð¢Ñ€Ð°Ð²ÐµÐ½ÑŒ", "Ð§ÐµÑ€Ð²ÐµÐ½ÑŒ", "Ð›Ð¸Ð¿ÐµÐ½ÑŒ", "Ð¡ÐµÑ€Ð¿ÐµÐ½ÑŒ", "Ð’ÐµÑ€ÐµÑÐµÐ½ÑŒ", "Ð–Ð¾Ð²Ñ‚ÐµÐ½ÑŒ", "Ð›Ð¸ÑÑ‚Ð¾Ð¿Ð°Ð´", "Ð“Ñ€ÑƒÐ´ÐµÐ½ÑŒ"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "vi": {
            "dayNames": ["Chá»§ Nháº­t", "Thá»© Hai", "Thá»© Ba", "Thá»© TÆ°", "Thá»© NÄƒm", "Thá»© SÃ¡u", "Thá»© Báº£y"],
            "dayNamesMin": ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["ThÃ¡ng Má»™t", "ThÃ¡ng Hai", "ThÃ¡ng Ba", "ThÃ¡ng TÆ°", "ThÃ¡ng NÄƒm", "ThÃ¡ng SÃ¡u", "ThÃ¡ng Báº£y", "ThÃ¡ng TÃ¡m", "ThÃ¡ng ChÃ­n", "ThÃ¡ng MÆ°á»i", "ThÃ¡ng MÆ°á»i Má»™t", "ThÃ¡ng MÆ°á»i Hai"],
            "showMonthAfterYear": false,
            "yearSuffix": ""
        },
        "zh-CN": {
            "dayNames": ["æ˜ŸæœŸæ—¥", "æ˜ŸæœŸä¸€", "æ˜ŸæœŸäºŒ", "æ˜ŸæœŸä¸‰", "æ˜ŸæœŸå››", "æ˜ŸæœŸäº”", "æ˜ŸæœŸå…­"],
            "dayNamesMin": ["å‘¨æ—¥", "å‘¨ä¸€", "å‘¨äºŒ", "å‘¨ä¸‰", "å‘¨å››", "å‘¨äº”", "å‘¨å…­"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["ä¸€æœˆ", "äºŒæœˆ", "ä¸‰æœˆ", "å››æœˆ", "äº”æœˆ", "å…­æœˆ", "ä¸ƒæœˆ", "å…«æœˆ", "ä¹æœˆ", "åæœˆ", "åä¸€æœˆ", "åäºŒæœˆ"],
            "showMonthAfterYear": true,
            "yearSuffix": "å¹´"
        },
        "zh-HK": {
            "dayNames": ["æ˜ŸæœŸæ—¥", "æ˜ŸæœŸä¸€", "æ˜ŸæœŸäºŒ", "æ˜ŸæœŸä¸‰", "æ˜ŸæœŸå››", "æ˜ŸæœŸäº”", "æ˜ŸæœŸå…­"],
            "dayNamesMin": ["å‘¨æ—¥", "å‘¨ä¸€", "å‘¨äºŒ", "å‘¨ä¸‰", "å‘¨å››", "å‘¨äº”", "å‘¨å…­"],
            "firstDay": 0,
            "isRTL": false,
            "monthNames": ["ä¸€æœˆ", "äºŒæœˆ", "ä¸‰æœˆ", "å››æœˆ", "äº”æœˆ", "å…­æœˆ", "ä¸ƒæœˆ", "å…«æœˆ", "ä¹æœˆ", "åæœˆ", "åä¸€æœˆ", "åäºŒæœˆ"],
            "showMonthAfterYear": true,
            "yearSuffix": "å¹´"
        },
        "zh-TW": {
            "dayNames": ["æ˜ŸæœŸæ—¥", "æ˜ŸæœŸä¸€", "æ˜ŸæœŸäºŒ", "æ˜ŸæœŸä¸‰", "æ˜ŸæœŸå››", "æ˜ŸæœŸäº”", "æ˜ŸæœŸå…­"],
            "dayNamesMin": ["å‘¨æ—¥", "å‘¨ä¸€", "å‘¨äºŒ", "å‘¨ä¸‰", "å‘¨å››", "å‘¨äº”", "å‘¨å…­"],
            "firstDay": 1,
            "isRTL": false,
            "monthNames": ["ä¸€æœˆ", "äºŒæœˆ", "ä¸‰æœˆ", "å››æœˆ", "äº”æœˆ", "å…­æœˆ", "ä¸ƒæœˆ", "å…«æœˆ", "ä¹æœˆ", "åæœˆ", "åä¸€æœˆ", "åäºŒæœˆ"],
            "showMonthAfterYear": true,
            "yearSuffix": "å¹´"
        }
    };

    // -------------------------------------------------------------------------
    // finally, integrate with jQuery for convenience --------------------------
    // -------------------------------------------------------------------------

    $.fn.datePicker = function (options) {
        return (new AJS.DatePicker(this, options));
    };

}(jQuery));
