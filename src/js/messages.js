(function (){

    var DEFAULT_FADEOUT_DURATION = 500;
    var DEFAULT_FADEOUT_DELAY = 5000;
    var FADEOUT_RESTORE_DURATION = 100;

    /**
     * Utility methods to display different message types to the user.
     * Usage:
     * <pre>
     * AJS.messages.info("#container", {
     *   title: "Info",
     *   body: "You can choose to have messages without Close functionality.",
     *   closeable: false,
     *   shadowed: false
     * });
     * </pre>
     * @class messages
     * @namespace AJS
     * @requires AJS.keyCode
     */
    AJS.messages = {
        setup: function () {
            AJS.messages.createMessage("generic");
            AJS.messages.createMessage("error");
            AJS.messages.createMessage("warning");
            AJS.messages.createMessage("info");
            AJS.messages.createMessage("success");
            AJS.messages.createMessage("hint");
            AJS.messages.makeCloseable();
            AJS.messages.makeFadeout();
        },
        makeCloseable: function (message) {
            AJS.$(message || "div.aui-message.closeable").each(function () {
                var $this = AJS.$(this),
                    $icon = AJS.$('<span class="aui-icon icon-close" role="button" tabindex="0"></span>').click(function () {
                        $this.closeMessage();
                    }).keypress(function (event) {
                        if ((event.which === AJS.keyCode.ENTER) || (event.which === AJS.keyCode.SPACE)) {
                            $this.closeMessage();
                            event.preventDefault(); // this is especially important when handling the space bar, as we don't want to page down
                        }
                    });
                $this.append($icon);
            });
        },
        makeFadeout: function (message, delay, duration) {
            delay = (typeof delay != "undefined") ? delay : DEFAULT_FADEOUT_DELAY;
            duration = (typeof duration != "undefined") ? duration : DEFAULT_FADEOUT_DURATION;

            AJS.$(message || "div.aui-message.fadeout").each(function () {
                var $this = AJS.$(this);

                //Store the component state to avoid collisions between animations
                var hasFocus = false;
                var isHover = false;

                //Small functions to keep the code easier to read and avoid code duplication
                function fadeOut(){
                    //Algorithm:
                    //1. Stop all running animations (first arg), including any fade animation and delay
                    //   Do not jump to the end of the animation (second arg). This prevents the message to abruptly
                    //   jump to opacity:0 or opacity:1
                    //2. Wait <delay> ms before starting the fadeout
                    //3. Start the fadeout with a duration of <duration> ms
                    //4. Close the message at the end of the animation
                    $this.stop(true,false).delay(delay).fadeOut(duration, function(){$this.closeMessage()});
                }
                function resetFadeOut(){
                    //Algorithm:
                    //1. Stop all running animations (first arg), including any fade animation and delay
                    //   Do not jump to the end of the animation (second arg). This prevents the message to abruptly
                    //   jump to opacity:0 or opacity:1
                    //2. Fast animation to opacity:1
                    $this.stop(true,false).fadeTo(FADEOUT_RESTORE_DURATION, 1);
                }
                function shouldStartFadeOut(){
                    return !hasFocus && !isHover;
                }

                //Attach handlers for user interactions (focus and hover)
                $this
                    .focusin(function(){
                        hasFocus = true;
                        resetFadeOut();
                    })
                    .focusout(function(){
                        hasFocus = false;
                        if (shouldStartFadeOut()) {
                            fadeOut();
                        }
                    })
                    .hover(
                        function(){  //should be called .hoverin(), but jQuery does not implement that method
                            isHover = true;
                            resetFadeOut();
                        },
                        function(){ //should be called .hoverout(), but jQuery does not implement that method
                            isHover = false;
                            if (shouldStartFadeOut()) {
                                fadeOut();
                            }
                        }
                    );

                //Initial animation
                fadeOut();
            });
        },
        template: '<div class="aui-message {type} {closeable} {shadowed} {fadeout}"><p class="title"><span class="aui-icon icon-{type}"></span><strong>{title}</strong></p>{body}<!-- .aui-message --></div>',
        createMessage: function (type) {
            AJS.messages[type] = function (context, obj) {
                var template = this.template,
                    $message,
                    insertWhere;

                if (!obj) {
                    obj = context;
                    context = "#aui-message-bar";
                }
               
                // Set up our template options
                obj.closeable = (obj.closeable != false);
                // shadowed no longer does anything but left in so it doesn't break
                obj.shadowed = (obj.shadowed != false);

                // Append the message using template
                $message = AJS.$(AJS.template(template).fill({
                    type: type,
                    closeable: obj.closeable ? "closeable" : "",
                    shadowed: obj.shadowed ? "shadowed" : "",
                    fadeout: obj.fadeout ? "fadeout" : "",
                    title: obj.title || "",
                    "body:html": obj.body || ""
                }).toString());
                
                // Add ID if supplied
                if (obj.id) {
                    if ( /[#\'\"\.\s]/g.test(obj.id) ) {
                        // reject IDs that don't comply with style guide (ie. they'll break stuff)
                        AJS.log("AJS.Messages error: ID rejected, must not include spaces, hashes, dots or quotes.");
                    } else {
                        $message.attr('id', obj.id);
                    }
                }

                // insertion option
                insertWhere = obj.insert || "append";

                // insert according to option or default
                if ( insertWhere === "prepend" ) {
                    $message.prependTo(context);
                } else {
                    $message.appendTo(context);
                }

                // Attach the optional extra behaviours
                obj.closeable && AJS.messages.makeCloseable($message);
                obj.fadeout && AJS.messages.makeFadeout($message, obj.delay, obj.duration);
                
                return $message;
            };
        }
    };

    AJS.$.fn.closeMessage = function () {
        var $message = AJS.$(this);
        if ($message.hasClass("aui-message", "closeable")) {
            $message.stop(true); //Stop any running animation
            $message.trigger("messageClose", [this]).remove();  //messageClose event Deprecated as of 5.3
            AJS.$(document).trigger("aui-message-close", [this]);  //must trigger on document since the element has been removed
        }
    };

    AJS.$(function () {AJS.messages.setup();});
})();
