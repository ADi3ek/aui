(function($) {

    /*
     IE8 doesn't support calc(), so we emulate all our calc stuff here. This needs to be kept in sync with the CSS.
     */
    function makeCalcShimEvents() {
        var openDialogs = [];
        var $window = $(window);
        var windowHeight;
        var windowWidth;
        var setDialogHeight = function(dialog) {
            // fast hasClass evaluation, seeing as this runs in a resize loop
            var dialogClass = " " + dialog.$el[0].className + " ";
            function dialogHasClass(selector) {
                return dialogClass.indexOf(" " + selector + " ") >= 0;
            };

            var dialogSize = dialogHasClass('aui-dialog2-small')   ? 'small'   :
                             dialogHasClass('aui-dialog2-medium')  ? 'medium'  :
                             dialogHasClass('aui-dialog2-large')   ? 'large'   :
                             dialogHasClass('aui-dialog2-xlarge')  ? 'xlarge'  :
                                                                     'custom';
            var dialogFitsWidth;
            var dialogFitsHeight;
            switch(dialogSize) {
                case 'small':
                    dialogFitsWidth = windowWidth > 420;
                    dialogFitsHeight = windowHeight > 500;
                    break;
                case 'medium':
                    dialogFitsWidth = windowWidth > 620;
                    dialogFitsHeight = windowHeight > 500;
                    break;
                case 'large':
                    dialogFitsWidth = windowWidth > 820;
                    dialogFitsHeight = windowHeight > 700;
                    break;
                case 'xlarge':
                    dialogFitsWidth = windowWidth > 1000;
                    dialogFitsHeight = windowHeight > 700;
                    break;
                default:
                    // custom sizers can do their own thing
                    dialogFitsWidth = true;
                    dialogFitsHeight = true;
            }
            dialog.$el
                .toggleClass('aui-dialog2-fullscreen', !dialogFitsWidth)
                .css('height', windowHeight - 107 - (dialogFitsWidth ? 200 : 0));
            dialog.$el.find('.aui-dialog2-content')
                .css('min-height', dialogFitsHeight ?   '' :
                                 windowHeight > 500 ? '193px' :
                                                      '93px');
        };
        var resizeHandler = function() {
            windowHeight = $window.height();
            windowWidth = $window.width();
            for(var i = 0, len = openDialogs.length; i < len; i++) {
                setDialogHeight(openDialogs[i]);
            }
        };

        var onShow = function (dialog) {
            if (!openDialogs.length) {
                windowHeight = $window.height();
                windowWidth = $window.width();
                $window.on('resize', resizeHandler);
            }
            setDialogHeight(dialog);
            openDialogs.push(dialog);
        };
        var onHide = function (dialog) {
            openDialogs = $.grep(openDialogs, function(openDialog) {
                return dialog !== openDialog;
            });
            if (!openDialogs.length) {
                $window.off('resize', resizeHandler);
            }
        };

        return {
            show: onShow,
            hide: onHide
        };
    }

    var calcShimEvents;

    function triggerCalcShimEvent(dialog, event) {
        if (!calcShimEvents) {
            calcShimEvents = AJS._internal.browser.supportsCalc() ? {} : makeCalcShimEvents();
        }
        calcShimEvents[event] && calcShimEvents[event](dialog);
    }

    var defaults = {
        "aui-focus-selector": ".aui-dialog2-content :input:visible:enabled",
        "aui-blanketed": "true"
    };

    function applyDefaults($el) {
        jQuery.each(defaults, function(key, value) {
            var dataKey = "data-" + key;
            if (!$el[0].hasAttribute(dataKey)) {
                $el.attr(dataKey, value);
            }
        });
    }

    function Dialog2(selector) {
        if (selector) {
            this.$el = $(selector);
        }
        else {
            this.$el = $(AJS.parseHtml($(aui.dialog.dialog2({}))));
        }
        applyDefaults(this.$el);
    }

    Dialog2.prototype.on = function(event, fn) {
        AJS.layer(this.$el).on(event, fn);
        return this;
    };
    
    Dialog2.prototype.show = function() {
        triggerCalcShimEvent(this, "show");
        AJS.layer(this.$el).show();
        return this;
    };

    Dialog2.prototype.hide = function() {
        triggerCalcShimEvent(this, "hide");
        AJS.layer(this.$el).hide();
        if (this.$el.data("aui-remove-on-hide")) {
            AJS.layer(this.$el).remove();
        }
        return this;
    };

    Dialog2.prototype.remove = function() {
        triggerCalcShimEvent(this, "hide");
        AJS.layer(this.$el).remove();
        return this;
    };

    AJS.dialog2 = AJS._internal.widget('dialog2', Dialog2);

    AJS.dialog2.on = function(eventName, fn) {
        AJS.layer.on(eventName, function(e, $el) {
            if ($el.is(".aui-dialog2")) {
                fn.apply(this, arguments);
            }
        });
    };

    /* Live events */

    $(document).on('click', '.aui-dialog2-header-close', function(e) {
        e.preventDefault();
        AJS.dialog2($(this).closest('.aui-dialog2')).hide();
    });

})(AJS.$);