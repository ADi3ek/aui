/**
 * @deprecated warnAboutFirebug As of AUI 5.1 this no longer works. 
 * Firebug actively blocked detection methods; and in any case the need for the feature is gone.
 */
AJS.warnAboutFirebug = function (message) {
    AJS.log("DEPRECATED: please remove all uses of AJS.warnAboutFirebug");
};
