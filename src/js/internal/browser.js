AJS._internal || ( AJS._internal = {});
(function($) {
    AJS._internal.browser = {};

    var supportsCalc = null;

    AJS._internal.browser.supportsCalc = function() {
        if (supportsCalc === null) {
            var $d = $('<div style="height: 10px; height: -webkit-calc(20px + 0); height: calc(20px);"></div>');
            supportsCalc = (20 === $d.appendTo(document.documentElement).height());
            $d.remove();
        }
        return supportsCalc;
    };
}(AJS.$));