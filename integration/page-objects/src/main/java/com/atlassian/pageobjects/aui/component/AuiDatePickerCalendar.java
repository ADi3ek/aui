package com.atlassian.pageobjects.aui.component;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.utils.by.ByJquery;
import com.atlassian.webdriver.waiter.Waiter;
import com.atlassian.webdriver.waiter.webdriver.function.ConditionFunction;
import com.google.common.base.Preconditions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * WARNING: don't instantiate directly. Get this via the {@link AuiDatePicker}
 *
 * @since 3.7
 *
 * @deprecated Previously published API deprecated as of AUI 5.0. Do not use outside AUI. Will be refactored out eventually.
 */
@Deprecated
public class AuiDatePickerCalendar
{
    @Inject
    PageElementFinder elementFinder;

    @Inject
    PageBinder binder;

    @Inject
    Waiter waiter;

    @Inject
    WebDriver driver;

    private final AuiDatePicker datePicker;
    private final String uuid;

    private PageElement calendar;

    public AuiDatePickerCalendar(AuiDatePicker datePicker, String uuid)
    {
        this.datePicker = datePicker;
        this.uuid = uuid;
    }

    @Init
    private void init()
    {
        this.calendar = elementFinder.find(By.cssSelector("div[data-aui-dp-popup-uuid='" + uuid + "']"));
        calendar.timed().isVisible().now();
    }

    /**
     * This de-focuses the date picker field
     * @return an instance of the {@link AuiDatePicker}
     */
    public AuiDatePicker close()
    {
        if (isOpen())
        {
            Actions action = new Actions(driver);

            WebElement el = driver.findElement(ByJquery.$("*[data-aui-dp-uuid=" + uuid + "]").parent());
            action.click(el).perform();

            waitForCalendarToClose();
        }
        return datePicker;
    }

    /**
     * Checks if the calendar is still open.
     */
    public boolean isOpen()
    {
        return calendar.isPresent() && calendar.isVisible();
    }

    /**
     * @return the current month that the date picker popup is set to.
     */
    public String getMonth()
    {
        Preconditions.checkState(isOpen(), "The calendar must be open to be able to get the month");
        return calendar.find(By.className("ui-datepicker-month")).getText();
    }

    /**
     * @return the current year that the date picker popup is set to.
     */
    public int getYear()
    {
        Preconditions.checkState(isOpen(), "The calendar must be open to be able to get the year");
        return Integer.parseInt(calendar.find(By.className("ui-datepicker-year")).getText());
    }

    /**
     * @return the current day the date picker popup is set to. -1 if there isn't one.
     */
    public int getDay()
    {
        Preconditions.checkState(isOpen(), "The calendar must be open to be able to get the day");
        PageElement el = calendar.find(By.className("ui-datepicker-current-day"));

        if (el.isPresent())
        {
            return Integer.parseInt(el.getText());
        }
        return -1;
    }

    /**
     * The date picker will change to the previous month.
     * @return an instance of the {@link AuiDatePickerCalendar}
     */
    public AuiDatePickerCalendar previousMonth()
    {
        Preconditions.checkState(isOpen(), "The calendar must be open to be able to change to the prev month");
        String currentMonth = getMonth();
        calendar.find(By.className("ui-datepicker-prev")).click();

        waitForMonthChange(currentMonth);

        return this;
    }

    public boolean canChangeToPreviousMonth()
    {
        return !calendar.find(By.className("ui-datepicker-prev")).hasClass("ui-state-disabled");
    }

    public boolean canChangeToNextMonth()
    {
        return !calendar.find(By.className("ui-datepicker-prev")).hasClass("ui-state-disabled");
    }

    /**
     * The date picker will change to the next month.
     * @return an instance of the {@link AuiDatePickerCalendar}
     */
    public AuiDatePickerCalendar nextMonth()
    {
        Preconditions.checkState(isOpen(), "The calendar must be open to be able to change to the next month");
        String currentMonth = getMonth();
        calendar.find(By.className("ui-datepicker-next")).click();

        waitForMonthChange(currentMonth);

        return this;
    }

    private void waitForMonthChange(final String currentMonth)
    {
        waiter.until().function(new ConditionFunction()
        {
            public Boolean apply(final WebDriver webDriver)
            {
                return currentMonth != getMonth();
            }
        }).isTrue().execute();
    }

    private void waitForCalendarToClose()
    {
        waiter.until().function(new ConditionFunction()
        {
            public Boolean apply(final WebDriver webDriver)
            {
                return !isOpen();
            }
        }).isTrue().execute();
    }

    /**
     * The date picker will select the day on the calendar that matches the value
     * passed in.
     * @param day the day to select on the calendar.
     * @return an instance of the {@link AuiDatePicker}
     */
    public AuiDatePicker setDay(int day)
    {
        Preconditions.checkState(isOpen(), "The calendar must be open to be able to set the day");
        PageElement el = calendar.find(
                ByJquery.$(".ui-datepicker-calendar td *:contains(" + day + ")").filter("function() { return ATLWD.$(this).text() == " + day + "}"));

        Preconditions.checkState(el.isPresent(),
                "The day: %d cannot be found for this date: %s/%s", day, getMonth(), getYear());

        Preconditions.checkState(!el.hasClass("ui-state-disabled"),
                "This day cannot be selected as it is disabled.");

        el.click();

        waitForCalendarToClose();

        return datePicker;
    }

    /**
     * Checks whether the supplied day can be set on the calendar.
     * This checks that both the day is present on the calendar and that
     * it is not disabled.
     * @param day the day to check
     * @return
     */
    public boolean canSetDay(int day)
    {
        Preconditions.checkState(isOpen(), "The calendar must be open.");
        PageElement el = calendar.find(
                ByJquery.$(".ui-datepicker-calendar td *:contains(" + day + ")").filter("function() { return ATLWD.$(this).text() == " + day + "}").parent());

        return el.isPresent() && !el.hasClass("ui-state-disabled");
    }

    public List<String> getDaysOfWeekShortNames()
    {
        Preconditions.checkState(isOpen(), "The calendar must be open to get the days of the week.");
        List<String> days = new ArrayList<String> ();
        List<PageElement> dayElements = calendar.findAll(By.cssSelector(".ui-datepicker-calendar thead th span"));

        for(PageElement dayEl : dayElements)
        {
            days.add(dayEl.getText());
        }

        return days;
    }

    public List<String> getDaysOfWeekLongNames()
    {
        Preconditions.checkState(isOpen(), "The calendar must be open to get the days of the week.");
        List<String> days = new ArrayList<String> ();
        List<PageElement> dayElements = calendar.findAll(By.cssSelector(".ui-datepicker-calendar thead th span"));

        for(PageElement dayEl : dayElements)
        {
            days.add(dayEl.getAttribute("title"));
        }

        return days;
    }
}
