package com.atlassian.pageobjects.aui;

import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.binder.InjectPageBinder;
import com.atlassian.pageobjects.binder.StandardModule;
import com.atlassian.pageobjects.elements.ElementModule;
import com.atlassian.pageobjects.elements.timeout.TimeoutsModule;
import com.atlassian.webdriver.AtlassianWebDriverModule;
import com.atlassian.webdriver.pageobjects.DefaultWebDriverTester;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since v3.7
 *
 * @deprecated Previously published API deprecated as of AUI 5.0. Do not use outside AUI. Will be refactored out eventually.
 */

@Deprecated
@Defaults (instanceId = "ajs", contextPath = "/ajs", httpPort = 9999)
public class AuiTestedProduct implements TestedProduct<WebDriverTester>
{
    private final PageBinder pageBinder;
    private final WebDriverTester webDriverTester;
    private final ProductInstance productInstance;

    public AuiTestedProduct(TestedProductFactory.TesterFactory<WebDriverTester> testerFactory,
            ProductInstance productInstance)
    {
        this.productInstance = checkNotNull(productInstance, "Product instance can not be null");
        WebDriverTester tester = null;
        if (testerFactory == null)
        {
            tester = new DefaultWebDriverTester();
        }
        else
        {
            tester = testerFactory.create();
        }

        this.webDriverTester = tester;
        this.pageBinder = new InjectPageBinder(productInstance, tester, new StandardModule(this),
                new AtlassianWebDriverModule(this), new ElementModule(), new TimeoutsModule());
    }

    @Override
    public PageBinder getPageBinder()
    {
        return pageBinder;
    }

    @Override
    public <P extends Page> P visit(final Class<P> pageClass, final Object... args)
    {
        return pageBinder.navigateToAndBind(pageClass, args);
    }

    @Override
    public ProductInstance getProductInstance()
    {
        return productInstance;
    }

    @Override
    public WebDriverTester getTester()
    {
        return webDriverTester;
    }
}
