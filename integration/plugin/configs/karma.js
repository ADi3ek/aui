module.exports = {
    frameworks: ['qunit', 'requirejs', 'sinon'],
    browsers: ['PhantomJS'],
    reporters: ['progress', 'junit'],
    junitReporter: {
        outputFile: 'target/surefire-reports/karma-results.xml',
        suite: ''
    },
    files: [
        // karma + requireJS config
        'configs/require.js',
        'configs/karma-main.js',

        // CSS
        'src/main/resources/css/atlassian/layer.css',
        'src/main/resources/css/atlassian/tabs.css',
        'src/test/resources/unit-tests/atlassian-js/atlassian-js-test.css',

        // unpacked
        {pattern: 'target/qunit/dependencies/jquery.js', included: false},
        {pattern: 'target/soyutils/js/soyutils.js', included: false},
        {pattern: 'target/qunit/soy/*.js', included: false},

        // main
        {pattern: 'src/main/resources/js/**/*.js', included: false},

        // test
        {pattern: 'src/test/resources/unit-tests/aui-qunit.js', included: false},
        {pattern: 'src/test/resources/unit-tests/{*,**/*}-test.js', included: false}
    ]
};
