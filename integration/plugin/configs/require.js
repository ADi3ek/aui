window.requireConfig = {
    baseUrl: '/src/main/resources/js',

    paths: {
        // Unpacked jquery
        'jquery': '../../../../target/qunit/dependencies/jquery',
        // Unpacked soy dep
        'soyutils': '../../../../target/soyutils/js/soyutils',
        // Compiled soy
        'soy/aui': '../../../../target/qunit/soy/atlassian.soy',
        'soy/avatar': '../../../../target/qunit/soy/avatar.soy',
        'soy/badges': '../../../../target/qunit/soy/badges.soy',
        'soy/buttons': '../../../../target/qunit/soy/buttons.soy',
        'soy/dialog2': '../../../../target/qunit/soy/dialog2.soy',
        'soy/dropdown': '../../../../target/qunit/soy/dropdown.soy',
        'soy/dropdown2': '../../../../target/qunit/soy/dropdown2.soy',
        'soy/expander': '../../../../target/qunit/soy/expander.soy',
        'soy/form': '../../../../target/qunit/soy/form.soy',
        'soy/group': '../../../../target/qunit/soy/group.soy',
        'soy/icons': '../../../../target/qunit/soy/icons.soy',
        'soy/labels': '../../../../target/qunit/soy/lables.soy',
        'soy/message': '../../../../target/qunit/soy/message.soy',
        'soy/pane': '../../../../target/qunit/soy/pane.soy',
        'soy/panel': '../../../../target/qunit/soy/panel.soy',
        'soy/progress-tracker': '../../../../target/qunit/soy/progress-tracker.soy',
        'soy/table': '../../../../target/qunit/soy/table.soy',
        'soy/tabs': '../../../../target/qunit/soy/tabs.soy',
        'soy/toolbar': '../../../../target/qunit/soy/toolbar.soy',
        'soy/toolbar2': '../../../../target/qunit/soy/toolbar2.soy',
        // Vendors
        'underscore': 'external/underscorejs/underscore',
        'backbone': 'external/backbone/backbone',
        'jquery.form': 'external/jquery/plugins/jquery.form',
        'jquery.aop': 'external/jquery/plugins/jquery.aop',
        'jquery.ui.datepicker': 'external/jquery/jquery-ui/jquery.ui.datepicker',
        'serializetoobject': 'external/jquery/serializetoobject',
        'html5-shim': 'external/html5-shim/html5',
        'jquery-select2': 'external/jquery/plugins/jquery.select2',
        // TODO: replace this with actual jquery ui deps
        'jquery-ui': 'external/jquery/jquery-ui/jquery-ui',

        // AUI
        'aui': 'atlassian/atlassian',
        'blanket': 'atlassian/blanket',
        'event': 'atlassian/event',
        'dialog': 'atlassian/dialog',
        'focus-manager': 'atlassian/focus-manager',
        'layer-manager': 'atlassian/layer-manager',
        'layer-manager-global': 'atlassian/layer-manager-global',
        'keyCode': 'atlassian/keyCode',
        'layer': 'atlassian/layer',
        'dialog2': 'atlassian/dialog2',
        'dropdown': 'atlassian/dropdown',
        'inline-dialog': 'atlassian/inline-dialog',
        'date-picker': 'atlassian/aui-date-picker',
        'format': 'atlassian/format',
        'events': 'atlassian/experimental-events/events',
        'forms': 'atlassian/forms',
        'template': 'atlassian/template',
        'messages': 'atlassian/messages',
        'select2': 'atlassian/aui-select2',
        'sidebar': 'atlassian/aui-experimental-sidebar',
        'tabs': 'atlassian/tabs',
        'toolbar': 'atlassian/toolbar',
        'jquery.hotkeys': 'atlassian/jquery/jquery.hotkeys',
        'whenitype': 'atlassian/whenitype',
        'autocomplete/progressive-data-set': 'atlassian/experimental-autocomplete/progressive-data-set',
        'autocomplete/query-input': 'atlassian/experimental-autocomplete/query-input',
        'autocomplete/query-result': 'atlassian/experimental-autocomplete/query-result',
        'autocomplete/truncating-progressive-data-set': 'atlassian/experimental-autocomplete/truncating-progressive-data-set',
        'internal/widget': 'atlassian/internal/widget',
        'internal/browser': 'atlassian/internal/browser',
        'restfultable': 'atlassian/experimental-restfultable/restfultable',
        'restfultable/entrymodel': 'atlassian/experimental-restfultable/restfultable.entrymodel',
        'restfultable/row': 'atlassian/experimental-restfultable/restfultable.row',
        'restfultable/editrow': 'atlassian/experimental-restfultable/restfultable.editrow',
        'restfultable/customview': 'atlassian/experimental-restfultable/restfultable.customview',
        'restfultable/all': 'atlassian/experimental-restfultable/restfultable.all',

        // test deps
        'aui-qunit': '../../../../src/test/resources/unit-tests/aui-qunit'
    },

    shim: {
        // vendor
        'backbone': ['jquery', 'underscore'],
        'jquery-ui': ['jquery'],
        'jquery.aop': ['jquery'],
        'jquery.hotkeys': ['jquery'],
        'serializetoobject': ['jquery'],
        'jquery-select2': ['jquery'],
        // aui
        'aui': ['jquery'],
        'blanket': ['aui'],
        'date-picker': ['aui', 'inline-dialog'],
        'dialog': ['aui', 'blanket', 'event'],
        'dialog2': ['aui', 'layer', 'layer-manager-global', 'internal/browser', 'internal/widget'],
        'dropdown': ['aui', 'jquery.aop'],
        'dropdown2': ['aui'],
        'event': ['aui'],
        'events': ['aui'],
        'format': ['aui'],
        'forms': ['aui'],
        'focus-manager': ['aui'],
        'inline-dialog': ['aui'],
        'keyCode': ['aui'],
        'layer': ['aui', 'focus-manager', 'internal/widget'],
        'layer-manager': ['aui', 'blanket', 'layer'],
        'layer-manager-global': ['aui', 'keyCode', 'layer-manager'],
        'messages': ['aui', 'template'],
        'select2': ['aui', 'jquery-select2'],
        'sidebar': ['aui', 'jquery-ui', 'underscore'],
        'tabs': ['aui'],
        'template': ['aui'],
        'toolbar': ['aui'],
        'whenitype': ['aui', 'format', 'keyCode', 'dropdown', 'jquery.hotkeys'],
        'internal/browser': ['aui'],
        'internal/widget': ['aui'],
        'autocomplete/progressive-data-set': ['aui', 'backbone'],
        'autocomplete/query-input': ['aui', 'backbone'],
        'autocomplete/query-result': ['aui', 'backbone'],
        'autocomplete/truncating-progressive-data-set': ['aui', 'autocomplete/progressive-data-set'],
        'restfultable': ['aui', 'backbone', 'events', 'format'],
        'restfultable/entrymodel': ['aui', 'backbone', 'events', 'restfultable'],
        'restfultable/row': ['aui', 'backbone', 'restfultable', 'soy/icons'],
        'restfultable/editrow': ['aui', 'backbone', 'restfultable', 'serializetoobject'],
        'restfultable/customview': ['aui', 'backbone', 'restfultable'],
        // restful table and its children have circular deps (restful table uses restfultable.*, restfultable.* requires
        // the restfultable namespace, so create a finaldep
        'restfultable/all': ['restfultable', 'restfultable/entrymodel', 'restfultable/row', 'restfultable/editrow', 'restfultable/customview'],
        // soy
        'soy/aui': ['soyutils'],
        'soy/avatar': ['soyutils', 'soy/aui'],
        'soy/badges': ['soyutils', 'soy/aui'],
        'soy/buttons': ['soyutils', 'soy/aui'],
        'soy/dialog2': ['soyutils', 'soy/aui'],
        'soy/dropdown': ['soyutils', 'soy/aui'],
        'soy/dropdown2': ['soyutils', 'soy/aui'],
        'soy/expander': ['soyutils', 'soy/aui'],
        'soy/form': ['soyutils', 'soy/aui'],
        'soy/group': ['soyutils', 'soy/aui'],
        'soy/icons': ['soyutils', 'soy/aui'],
        'soy/labels': ['soyutils', 'soy/aui'],
        'soy/message': ['soyutils', 'soy/aui'],
        'soy/pane': ['soyutils', 'soy/aui'],
        'soy/panel': ['soyutils', 'soy/aui'],
        'soy/progress-tracker': ['soyutils', 'soy/aui'],
        'soy/table': ['soyutils', 'soy/aui'],
        'soy/tabs': ['soyutils', 'soy/aui'],
        'soy/toolbar': ['soyutils', 'soy/aui'],
        'soy/toolbar2': ['soyutils', 'soy/aui'],
        // test
        'aui-qunit': ['aui']
    }
};
