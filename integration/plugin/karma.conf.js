module.exports = function(karma) {
    var config = require('./configs/karma');
    config.singleRun = false;
    config.autoWatch = true;
    karma.set(config);
};
