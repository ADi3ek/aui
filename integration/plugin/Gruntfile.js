module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        karma: {
            options: require('./configs/karma'),
            cli: {
                options: {
                    singleRun: true
                }
            },
            server: {}
        }
    });


    grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('test', 'Runs the sandbox unit tests.', ['generate-test-resources', 'karma:cli']);
    grunt.registerTask('server-test', 'Runs the sandbox unit test server for development', ['generate-test-resources', 'karma:server']);
    grunt.registerTask('generate-test-resources', function() {
        var exec = require('child_process').exec;
        var spawn = require('child_process').spawn;
        var done = this.async();

        function execHandler(err, out) {
            if (err) {
                grunt.log.error(err);
            }

            grunt.verbose.ok(out);
            done();
        }

        grunt.log.writeln('Running mvn install...');
        var install = spawn('mvn', [
            'install',
            '-DskipTests',
            '-DskipAllPrompts=true'
        ], {
            cwd: '..'
        });

        install.stdout.on('data', function (data) {
            console.log('stdout: ' + data);
        });

        install.stderr.on('data', function (data) {
            console.log('stderr: ' + data);
        });

        install.on('close', function() {
            grunt.log.writeln('Generating test resources...');
            exec('mvn generate-test-resources', execHandler);
        });
    });
};
