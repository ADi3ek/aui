AJS.$(function() {
	var body = AJS.$("body"),
		header,
		content,
		footer,
		pageContent;

	pageContent = AJS.$("#contentSource").html();
	AJS.$("#contentSource").remove();

	header = atlassian.page.header({
        logo : 'aui',
        headerLogoText : 'AUI',
        headerLink: AJS.contextPath(),
        primaryNavContent: '<ul class="aui-nav"> <li><a href="bootcamp.html">Bootcamp Page</a></li> <li><a href="bootcamp-example.html">Bootcamp Example Page</a></li> <li><a href="/ajs/plugins/servlet/ajstest/sandbox/">Sandbox</a></li> </ul>'
    });

	body.append(atlassian.page.page({
		headerContent: header,
		contentContent: pageContent,
		footerContent: '<p>I &hearts; AUI</p>'
	}));

});