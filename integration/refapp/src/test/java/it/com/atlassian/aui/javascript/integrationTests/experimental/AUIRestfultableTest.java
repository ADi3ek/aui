package it.com.atlassian.aui.javascript.integrationTests.experimental;

import com.atlassian.pageobjects.aui.component.restfultable.EditRow;
import com.atlassian.pageobjects.aui.component.restfultable.RestfulTable;
import com.atlassian.pageobjects.aui.component.restfultable.Row;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.refapp.page.RefappLoginPage;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.ResetRestfulTablePage;
import it.com.atlassian.aui.javascript.pages.RestfulTablePage;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AUIRestfultableTest extends AbstractAuiIntegrationTest
{

    RestfulTablePage restfulTablePage;
    private PageElementFinder elementFinder;


    @Before
    public void setup()
    {
        product.visit(ResetRestfulTablePage.class);
        restfulTablePage = product.visit(RefappLoginPage.class).loginAsSysAdmin(RestfulTablePage.class);
        elementFinder = restfulTablePage.getElementFinder();
    }

    @Test
    public void testSuccessfullyCreatingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        Poller.waitUntilTrue(table.hasAnyRows());
        Row addedRow = table.getFirstRow();
        assertEquals("Scott", addedRow.getValue("name"));
        assertEquals("Work", addedRow.getValue("group"));
        assertEquals("0412947430", addedRow.getValue("number"));
    }

    @Test
    public void createRowIsEmptyAfterSuccessfulCreate()
    {
        RestfulTable table = restfulTablePage.getTable();
        EditRow createRow = table.getCreateRow();
        assertEquals("Friends", createRow.getValue("group")); // Check the default value is 'friends'

        // Fill in some data
        createRow.fill("name", "Chris", "group", "Work", "number", "0412345678");
        assertEquals("Chris", createRow.getValue("name"));
        assertEquals("Work", createRow.getValue("group")); // Shouldn't be the default value
        assertEquals("0412345678", createRow.getValue("number"));

        // Submit the data
        createRow.submit();
        Poller.waitUntilTrue(table.hasAnyRows());
        createRow = table.getCreateRow(); // Get it again, just in case

        assertEquals("", createRow.getValue("name"));
        assertEquals("Friends", createRow.getValue("group")); // The default value
        assertEquals("", createRow.getValue("number"));
    }

    @Test
    public void testValidationErrorCreatingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        final EditRow createRow = table.getCreateRow();
        createRow.fill("name", "Scott", "group", "Work", "number", "invalidno")
                .submit();
        assertTrue(createRow.getValidationErrors().containsValue("Not a valid number"));
    }

    @Test
    public void testDeletingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        Poller.waitUntilTrue(table.hasAnyRows());
        final Row firstRow = table.getFirstRow();
        firstRow.delete();
        Poller.waitUntilFalse(table.hasAnyRows());
        assertTrue(table.isEmpty());
    }

    @Ignore("Flaky test. Manual check passes. https://ecosystem.atlassian.net/browse/AUI-1399")
    @Test
    public void testEditingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        Row firstRow = table.getFirstRow();
        EditRow edit = firstRow.edit("group").setFieldValue("group", "family").submit();
        assertEquals("Family", firstRow.getValue("group"));
    }

    @Test
    public void testCustomFocusSelector()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        Row firstRow = table.getFirstRow();
        EditRow row = firstRow.edit("checkbox");
        String javascript = "return AJS.$(arguments[0]).is(':focus');";
        Boolean hasFocus = (Boolean) row.getPageElement().find(By.className("ajs-restfultable-input-checkbox")).javascript().execute(javascript);
        assertTrue(hasFocus.booleanValue());
    }

    @Test
    public void testValidationErrorEditingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        Poller.waitUntilTrue(table.hasAnyRows());
        Row firstRow = table.getFirstRow();
        EditRow editRow = firstRow.edit("number").setFieldValue("number", "dgasdgsd").submit();
        assertTrue(editRow.getValidationErrors().containsValue("Not a valid number"));
    }

    @Test
    public void testCancelEditingEntryLeavesFieldValuesUnchanged()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        Poller.waitUntilTrue(table.hasAnyRows());
        // Change just the one thing
        EditRow editRow = table.getFirstRow().edit("name").setFieldValue("name", "Chris");
        editRow.cancel();
        assertEquals("Scott", table.getFirstRow().getValue("name"));
        // Change two things
        editRow = table.getFirstRow().edit("name").setFieldValue("name", "Chris");
        editRow.setFieldValue("group", "Friends");
        editRow.cancel();
        assertEquals("Scott", table.getFirstRow().getValue("name"));
        assertEquals("Work", table.getFirstRow().getValue("group"));
    }

    @Test
    public void testAuiEventsFired()
    {
        //create
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        Poller.waitUntilTrue(table.hasAnyRows());

        //edit
        Row firstRow = table.getFirstRow();
        firstRow.edit("group").setFieldValue("group", "family").submit();

        //delete
        firstRow.delete();
        Poller.waitUntilFalse(table.hasAnyRows());

        PageElement rowAdded = elementFinder.find(By.cssSelector("#ROW_ADDED"));
        //Can't simulate drag events so this is commented.
        //PageElement reorderSuccess = elementFinder.find(By.cssSelector("#REORDER_SUCCESS"));
        PageElement rowRemoved = elementFinder.find(By.cssSelector("#ROW_REMOVED"));
        PageElement editRow = elementFinder.find(By.cssSelector("#EDIT_ROW"));

        assertTrue(rowAdded.isPresent());
        //assertTrue(reorderSuccess.isPresent());
        assertTrue(rowRemoved.isPresent());
        assertTrue(editRow.isPresent());
    }

    public void testAddPositionDefault()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        table.waitForRow(1);
        table.addEntry("name", "Mike", "group", "Work", "number", "0412947431");
        table.waitForRow(2);
        table.addEntry("name", "Ben", "group", "Work", "number", "0412947432");
        table.waitForRow(3);
        assertEquals("Ben", table.getFirstRow().getValue("name"));
        assertEquals("Scott", table.getRow(3).getValue("name"));
    }

    @Test
    public void testAddPositionAtTheBottom()
    {
        RestfulTable table = restfulTablePage.getTableAddPositionBottom();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        table.waitForRow(1);
        table.addEntry("name", "Mike", "group", "Work", "number", "0412947431");
        table.waitForRow(2);
        table.addEntry("name", "Ben", "group", "Work", "number", "0412947432");
        table.waitForRow(3);
        assertEquals("Scott", table.getFirstRow().getValue("name"));
        assertEquals("Ben", table.getRow(3).getValue("name"));
    }
}
