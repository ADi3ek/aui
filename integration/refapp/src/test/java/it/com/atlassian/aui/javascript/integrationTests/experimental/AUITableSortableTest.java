package it.com.atlassian.aui.javascript.integrationTests.experimental;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.TableSortableTestPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.lang.Comparable;import java.lang.Integer;import java.lang.String;import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Written by Jaiden Ashmore 21/Jan/2013
 */
public class AUITableSortableTest extends AbstractAuiIntegrationTest
{
    private PageElementFinder elementFinder = null;
    private static final String ORIGINAL_TABLE_ID = "sortableTable";
    private static final int ISSUE_KEY_INDEX = 0;
    private static final int ISSUE_NAME_INDEX = 1;
    private static final int WATCHER_INDEX = 2;

    private static final String DELAYED_TABLE_ID = "delayedSortedTable";
    private static final int DELAYED_NAME_INDEX = 0;

    @Before
    public void openTestPage()
    {
        TableSortableTestPage tableSortableTestPage = product.visit(TableSortableTestPage.class);
        elementFinder = tableSortableTestPage.getElementFinder();
    }

    @Test
    public void issueKeySortAscendingOrderTest()
    {
        PageElement originalTable = getTable(ORIGINAL_TABLE_ID);
        clickHeader(originalTable, ISSUE_KEY_INDEX, 1);
        List<String> issueKeyContent = getColumnRowsContentAsString(originalTable, ISSUE_KEY_INDEX);
        assertEquals("Issue keys are not in the correct descending order", issueKeyContent, new ArrayList<String>(Arrays.asList("BAT-4", "BAT-11", "BATA-8", "BATA-14", "KAR-4", "KAR-87", "KAR-1123", "KAR-1133")));
    }

    @Test
    public void issueKeySortDescendingOrderTest()
    {
        PageElement originalTable = getTable(ORIGINAL_TABLE_ID);
        clickHeader(originalTable, ISSUE_KEY_INDEX, 2);
        List<String> issueKeyContent = getColumnRowsContentAsString(originalTable, ISSUE_KEY_INDEX);
        assertEquals("Issue keys are not in the correct descending order", issueKeyContent, new ArrayList<String>(Arrays.asList("KAR-1133", "KAR-1123", "KAR-87", "KAR-4", "BATA-14", "BATA-8", "BAT-11", "BAT-4")));
    }

    @Test
    public void textSortAscendingOrderTest()
    {
        PageElement originalTable = getTable(ORIGINAL_TABLE_ID);
        clickHeader(originalTable, ISSUE_NAME_INDEX, 1);
        List<String> issueNameContent = getColumnRowsContentAsString(originalTable, ISSUE_NAME_INDEX);
        assertTrue("String column in sortable table is not in the correct descending order", inAscendingOrder(issueNameContent));
    }

    @Test
    public void textSortDescendingOrderTest()
    {
        PageElement originalTable = getTable(ORIGINAL_TABLE_ID);
        clickHeader(originalTable, ISSUE_NAME_INDEX, 2);
        List<String> issueNameContent = getColumnRowsContentAsString(originalTable, ISSUE_NAME_INDEX);
        assertTrue("Text column in sortable table is not in the correct descending order", inDescendingOrder(issueNameContent));
    }

    @Test
    public void numberSortAscendingOrderTest()
    {
        PageElement originalTable = getTable(ORIGINAL_TABLE_ID);
        clickHeader(originalTable, WATCHER_INDEX, 1);
        List<Integer> watchers = getColumnRowsContentAsInteger(originalTable, WATCHER_INDEX);
        assertTrue("Number column (watchers) in sortable table is not in the correct ascending order", inAscendingOrder(watchers));
    }

    @Test
    public void numberSortDescendingOrderTest()
    {
        PageElement originalTable = getTable(ORIGINAL_TABLE_ID);
        clickHeader(originalTable, WATCHER_INDEX, 2);
        List<Integer> watchers = getColumnRowsContentAsInteger(originalTable, WATCHER_INDEX);
        assertTrue("Number column (watchers) in sortable table is not in the correct descending order", inDescendingOrder(watchers));
    }

    /**
     * Tests when a table is added after the page DOM and js has been executed and wants to be sorted
     */
    @Test
    public void sortableTableAddedViaJavascriptTest()
    {
        PageElement delayedTable = getTable(DELAYED_TABLE_ID);
        clickHeader(delayedTable, DELAYED_NAME_INDEX, 1);
        List<String> nameContent = getColumnRowsContentAsString(delayedTable, DELAYED_NAME_INDEX);
        assertEquals(nameContent, new ArrayList<String>(Arrays.asList("Ben", "Jaiden", "Timmy")));

    }

    private PageElement getTable(String tableId)
    {
        return elementFinder.find(By.id(tableId));
    }

    private void clickHeader(PageElement table, int headerIndex, int numberClicks)
    {
        PageElement header = table.findAll(By.tagName("th")).get(headerIndex);
        for (int click = 0; click < numberClicks; ++click)
        {
            header.click();
        }
    }

    private List<String> getColumnRowsContentAsString(PageElement table, int columnIndex)
    {
        List<String> columnRowsContent = new ArrayList<String>();
        PageElement body = table.find(By.tagName("tbody"));
        List<PageElement> rows = body.findAll(By.tagName("tr"));
        for (PageElement row : rows) {
            PageElement rowElement = (row.findAll(By.tagName("td")).get(columnIndex));
            columnRowsContent.add(rowElement.getText());
        }
        return columnRowsContent;
    }

    private List<Integer> getColumnRowsContentAsInteger(PageElement table, int columnIndex)
    {
        List<Integer> columnRowsContentAsInteger = new ArrayList<Integer>();
        for (String rowContent : getColumnRowsContentAsString(table, columnIndex))
        {
            columnRowsContentAsInteger.add(Integer.parseInt(rowContent));
        }
        return columnRowsContentAsInteger;
    }

    private <T extends Comparable<T>> boolean inAscendingOrder(List<T> list)
    {
        List<T> sortedList = new ArrayList<T>(list);
        Collections.sort(sortedList);
        return sortedList.equals(list);
    }

    private <T extends Comparable<T>> boolean inDescendingOrder(List<T> list)
    {
        List<T> sortedList = new ArrayList<T>(list);
        Collections.sort(sortedList);
        Collections.reverse(sortedList);
        return sortedList.equals(list);
    }
}
