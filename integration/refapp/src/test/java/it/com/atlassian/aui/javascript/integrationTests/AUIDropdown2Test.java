package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.aui.component.ElementBounding;
import com.atlassian.pageobjects.aui.component.dropdown2.AuiDropdown2;
import com.atlassian.pageobjects.aui.component.dropdown2.AuiDropdown2Item;
import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.webdriver.testing.annotation.IgnoreBrowser;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.Dropdown2TestPage;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AUIDropdown2Test extends AbstractAuiIntegrationTest
{

    private Dropdown2TestPage dropdown2TestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setUp() {
        dropdown2TestPage = product.visit(Dropdown2TestPage.class);
        dropdown2TestPage.getCustomDropdownContainerElement().click();
        elementFinder = dropdown2TestPage.getElementFinder();
    }

    @Test
    public void testClickingTriggerOpensDropdown() {
        AuiDropdown2 dropdown = dropdown2TestPage.getStandardDropdown();
        assertFalse("The aui dropdown should not be open", dropdown.isOpen());
        dropdown.open();
        assertTrue("The aui dropdown should be open", dropdown.isOpen());
    }

    @Test
    public void testDrodownCloses() {
        AuiDropdown2 dropdown = dropdown2TestPage.getStandardDropdown();
        assertFalse("The aui dropdown should not be open", dropdown.isOpen());
        dropdown.open();
        assertTrue("The aui dropdown should be open", dropdown.isOpen());
        dropdown.close();
        assertFalse("The aui dropdown should not be open", dropdown.isOpen());
    }

    @Test
    public void testDrodownClosesWhenEscapeIsPressed() {
        AuiDropdown2 dropdown = dropdown2TestPage.getStandardDropdown();
        assertFalse("The aui dropdown should not be open", dropdown.isOpen());
        dropdown.open();
        assertTrue("The aui dropdown should be open", dropdown.isOpen());
        dropdown.closeWithEscape();
        assertFalse("The aui dropdown should not be open", dropdown.isOpen());
    }

    @Test
    public void testDrodownMatchesWidth() {
        AuiDropdown2 dropdown = dropdown2TestPage.getStandardDropdown();
        dropdown.open();
        int triggerWidth = dropdown.getTriggerWidth();
        int dropdownWidth = dropdown.getDropdownWidth();
        assertEquals("Width of trigger and dropdown should match ", triggerWidth, dropdownWidth);
    }

    @Test
    public void testCustomDropdownClipsLongText()
    {
        AuiDropdown2 dropdown = dropdown2TestPage.getCustomDropdown();
        dropdown.open();
        
        AuiDropdown2Item dropdownItem = dropdown.getDropdownItems().get(1);
        
        assertTrue("The 2nd item of the custom dropdown should be clipped.", dropdownItem.isClipped());
    }

    @Test
    public void testDisabledDropdownToggles()
    {
        AuiDropdown2 dropdown = dropdown2TestPage.getDisabledToolbarDropdown();

        // should be disabled on load (as in, disabled via dom)
        assertTrue("The dropdown is expected to be disabled.", dropdown.isDisabled());
        dropdown.open(false);
        assertFalse("The aui dropdown should not be open", dropdown.isOpen());

        // check it can be enabled
        dropdown2TestPage.getDisableDropdownButton().click();
        assertFalse("The dropdown should not be disabled.", dropdown.isDisabled());
        dropdown.open();
        assertTrue("The aui dropdown should be open", dropdown.isOpen());

        // check it can be disabled again
        dropdown2TestPage.getDisableDropdownButton().click();
        assertTrue("The dropdown is expected to be disabled.", dropdown.isDisabled());
        dropdown.open(false);
        assertFalse("The aui dropdown should not be open", dropdown.isOpen());

    }

    @Test
    @IgnoreBrowser(value= Browser.FIREFOX, reason="Firefox mysteriously failing on buildbox.")
    public void testIEAlignment()
    {
        AuiDropdown2 dropdown = dropdown2TestPage.getDropdownIEAlignmentTest();

        // open the dropdown
        dropdown.open();
        assertTrue("The aui dropdown should be open", dropdown.isOpen());

        // test the dropdown alignment is 'right'
        assertTrue("The dropdown should be right-aligned.", dropdown.getAlignment().equals("right"));
    }

    @Test
    public void testCustomBoundaryElementControlsDropdownAlignment()
    {
        AuiDropdown2 dropdown = dropdown2TestPage.getCustomContainedDropdown();
        WebDriverElement dropdownContainer = (WebDriverElement) dropdown2TestPage.getCustomDropdownContainerElement();

        dropdown.open();

        ElementBounding dropdownBounding = dropdown.getDropdownBounding();
        ElementBounding containerBounding = new ElementBounding(dropdownContainer);

        assertTrue(containerBounding.contains(ElementBounding.BoundingPosition.RIGHT, dropdownBounding));
    }

    @Test
    public void testUnstyledDropdownIsUnstyled()
    {
        AuiDropdown2 dropdown = dropdown2TestPage.getUnstyledDropdown();
        dropdown.open();
        WebElement dropdownElement = product.getTester().getDriver().findElement(By.id("dropdown2-unstyled"));
        assertEquals("The dropdown should have a transparent/unset background.", "transparent", dropdownElement.getCssValue("background-color"));
    }

    @Test
    public void testDropdownFauxRadioButtons()
    {
        AuiDropdown2 dropdown = dropdown2TestPage.getFauxFormDropdown();
        dropdown.open();
        assertTrue("The aui dropdown should be open", dropdown.isOpen());

//        AuiDropdown2Item dropdownCheck1 = dropdown.getDropdownItems().get(1);
//        AuiDropdown2Item dropdownCheck2 = dropdown.getDropdownItems().get(2);
//        AuiDropdown2Item dropdownCheck3 = dropdown.getDropdownItems().get(3);

//        assertTrue("Check 1 should be ticked.", dropdownCheck1.ariaChecked());
//        assertFalse("Check 2 should not be ticked.", dropdownCheck2.ariaChecked());
//        assertFalse("Check 3 should not be ticked.", dropdownCheck3.ariaChecked());

        WebElement dropdownRadio1 = product.getTester().getDriver().findElement(By.id("dropdown2-faux-forms-checkbox1"));
        WebElement dropdownRadio2 = product.getTester().getDriver().findElement(By.id("dropdown2-faux-forms-checkbox2"));
        WebElement dropdownRadio3 = product.getTester().getDriver().findElement(By.id("dropdown2-faux-forms-checkbox3"));

        assertEquals("Item 1 should be aria-checked=true.", "true", dropdownRadio1.getAttribute("aria-checked"));
        assertEquals("Item 2 should be aria-checked=false.", "false", dropdownRadio2.getAttribute("aria-checked"));
        assertEquals("Item 3 should be aria-checked=false.", "false", dropdownRadio3.getAttribute("aria-checked"));

        dropdownRadio2.click();
        assertTrue("The aui dropdown should still be open after radio button item has been clicked.", dropdown.isOpen());

        assertEquals("Item 1 should be aria-checked=false.", "false", dropdownRadio1.getAttribute("aria-checked"));
        assertEquals("Item 2 should be aria-checked=true.", "true", dropdownRadio2.getAttribute("aria-checked"));
        assertEquals("Item 3 should be aria-checked=false.", "false", dropdownRadio3.getAttribute("aria-checked"));

    }

    @Test
    public void testDropdownCustomHide()
    {
        PageElement dropdownTrigger = elementFinder.find(By.cssSelector("#dropdown2-custom-hide-trigger"));
        PageElement somethingToClick = elementFinder.find(By.id("dropdown2-hiding-test"));
        PageElement originalHidingPlace = elementFinder.find(By.cssSelector("#original-hiding-place"));
        PageElement hidemehere = elementFinder.find(By.cssSelector("#hidemehere"));

        assertTrue("There should be a custom hide dropdown in the original hiding place",
                originalHidingPlace.findAll(By.cssSelector("#dropdown2-custom-hide")).size() == 1);

        dropdownTrigger.click();
        somethingToClick.click();

        assertTrue("There should be a custom hide dropdown in hidemehere",
                hidemehere.findAll(By.cssSelector("#dropdown2-custom-hide")).size() == 1);

        assertTrue("There should be no custom hide dropdown in the original hiding place",
                originalHidingPlace.findAll(By.cssSelector("#dropdown2-custom-hide")).size() == 0);

    }

    @Test
    public void testDropdownDefaultHide()
    {
        PageElement dropdownTrigger = elementFinder.find(By.cssSelector("#dropdown2-default-hide-trigger"));
        PageElement somethingToClick = elementFinder.find(By.id("dropdown2-hiding-test"));
        PageElement originalHidingPlace = elementFinder.find(By.cssSelector("#original-hiding-place"));

        assertTrue("There should be a default hide dropdown in the original hiding place",
                originalHidingPlace.findAll(By.cssSelector("#dropdown2-default-hide")).size() == 1);

        dropdownTrigger.click();
        somethingToClick.click();

        assertTrue("There should be a default hide dropdown in the original hiding place",
                originalHidingPlace.findAll(By.cssSelector("#dropdown2-default-hide")).size() == 1);

    }

    @Test
    public void testDropdownFauxCheckboxes()
    {
        AuiDropdown2 dropdown = dropdown2TestPage.getFauxFormDropdown();
        dropdown.open();
        assertTrue("The aui dropdown should be open", dropdown.isOpen());

        WebElement dropdownCheck1 = product.getTester().getDriver().findElement(By.id("spellcheck"));

        assertEquals("Item 1 should be aria-checked=true.", "true", dropdownCheck1.getAttribute("aria-checked"));

        dropdownCheck1.click();
        assertTrue("The aui dropdown should still be open after checkbox has been clicked.", dropdown.isOpen());
        assertEquals("Item 1 should be aria-checked=false.", "false", dropdownCheck1.getAttribute("aria-checked"));
    }

    @Test
    @Ignore("ByJquery is currently not working for webelement context calls. Should be fixed in Webdriver 2.1.")
    public void testDropdownGroup()
    {
        AuiDropdown2 dropdown1 = dropdown2TestPage.getDropdownGroupTrigger1();

        assertTrue("The aui dropdown should be in a group", dropdown1.isInDropdownGroup());

        dropdown1.open();

        AuiDropdown2 dropdown2 = dropdown1.getNextDropdownInGroup();
        assertTrue("The second dropdown should be open after RIGHT ARROW keypress", dropdown2.isOpen());

    }

}
