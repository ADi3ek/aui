package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.aui.component.AuiDatePicker;
import com.atlassian.pageobjects.aui.component.AuiDatePickerCalendar;
import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.webdriver.testing.annotation.IgnoreBrowser;
import com.google.common.collect.ImmutableList;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.DatePickerTestPage;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AUIDatePickerTest extends AbstractAuiIntegrationTest
{

    private DatePickerTestPage datePickerPage;

    @Before
    public void setup()
    {
        datePickerPage = product.visit(DatePickerTestPage.class);
    }

    @Test
    public void testDatePickerPopupDisplaysOnFieldFocus() {
        AuiDatePickerCalendar calendar = datePickerPage.getBasicDatePicker().open();
        assertTrue(calendar.isOpen());
    }

    @Test
    @IgnoreBrowser (value=Browser.FIREFOX, reason="Firefox has a focus issue when the browser window is not active.")
    public void testDatePickerPopupHidesOnClose()
    {
        AuiDatePickerCalendar calendar = datePickerPage.getBasicDatePicker().open();

        assertTrue(calendar.isOpen());

        calendar.close();
        assertFalse(calendar.isOpen());
    }

    @Test
    public void testDatePickerPopulatesFieldOnSelection() {
        AuiDatePicker datePicker = datePickerPage.getDefaultDatePicker().open().setDay(31);
        assertEquals("2012-01-31", datePicker.getDate());
        assertFalse("The date picker should be closed", datePicker.isOpen());
    }

    @Test
    @IgnoreBrowser(value=Browser.FIREFOX, reason="Firefox has a focus issue when the browser window is not active.")
    public void testDatePickerInitiallySelectsDefaultValue() {

        AuiDatePickerCalendar calendar = datePickerPage.getDefaultDatePicker().open();

        assertEquals("The expected default month is January", "January", calendar.getMonth());
        assertEquals("The expected default year is 2012", 2012, calendar.getYear());
        assertEquals("The expected default day is 1", 1, calendar.getDay());

        assertEquals("The expected default date is 2012-01-01", "2012-01-01", calendar.close().getDate());
    }

    @Test
    public void testDatePickerDisplaysCorrectRange() {

        AuiDatePickerCalendar calendar = datePickerPage.getRangeDatePicker().open();

        assertEquals("Expected to be in January", "January", calendar.getMonth());
        assertEquals("Expected to be in 2012", 2012, calendar.getYear());
        assertTrue("Should be able to set the date as the 10th", calendar.canSetDay(10));
        assertTrue("Should be able to set the date as the 15th", calendar.canSetDay(15));
        assertTrue("Should be able to set the date as the 25th", calendar.canSetDay(25));
        assertFalse("Should not be able to set the date as the 9th", calendar.canSetDay(9));
        assertFalse("Should not be able to set the date as the 26th", calendar.canSetDay(26));

        assertFalse("Should not be able to select the next month", calendar.canChangeToNextMonth());
        assertFalse("Should not be able to select the previous month", calendar.canChangeToPreviousMonth());

        AuiDatePicker picker = calendar.setDay(15);

        assertEquals("Expected the date to be 2012-01-15", "2012-01-15", picker.getDate());
    }

    @Test
    public void testDatePickerRespectsCustomFirstDay() {

        AuiDatePickerCalendar calendar = datePickerPage.getDifferentStartDayDatePicker().open();

        assertEquals("Month should be January", "January", calendar.getMonth());
        assertEquals("Year should be 2012", 2012, calendar.getYear());

        assertEquals("First day in the week should be Wed",
                ImmutableList.of("Wed", "Thu", "Fri", "Sat", "Sun", "Mon", "Tue"),
                calendar.getDaysOfWeekShortNames());

        assertEquals("First day in the week should be Wednesday",
                ImmutableList.of("Wednesday", "Thursday", "Friday","Saturday", "Sunday", "Monday", "Tuesday"),
                calendar.getDaysOfWeekLongNames());
    }

    @Test
    public void testDatePickerRespectsCustomLocale() {

        AuiDatePickerCalendar calendar = datePickerPage.getCustomLanguageDatePicker().open();

        assertEquals("Month should be Janvier", "Janvier", calendar.getMonth());
        assertEquals("Year should be 2012", 2012, calendar.getYear());

        assertEquals("Days should be in french and start at Mon (Lun.).",
                ImmutableList.of("Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam.", "Dim."),
                calendar.getDaysOfWeekShortNames());

        assertEquals("Days should be in french and start at Monday (Lundi)",
                ImmutableList.of("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"),
                calendar.getDaysOfWeekLongNames());
    }

}
