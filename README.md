AUI
===

The Atlassian User Interface library.

Installation
------------

You need to install NPM packages and Maven dependencies:

    npm install && grunt install

Building
--------

To build run ensure everything is installed then run:

    grunt build

Running Tests
-------------

We use [Karma](http://karma-runner.github.io/0.10/index.html) for running our [Qunit](http://qunitjs.com/) tests.

You MUST install prior to running the tests. To run the tests:

    grunt test

Commands
--------

To see a wider range of Grunt commands run:

    grunt

How do you get it?
------------------

It's distributed in two ways:

- as an [Atlassian plugin](https://developer.atlassian.com/display/DOCS/Plugin+Framework) for use in Atlassian plugin-based products.
- as a [_flat pack_ distribution](https://docs.atlassian.com/aui/latest/) for use in any web app.

To prototype, you can use the online [Sandbox Tool](http://docs.atlassian.com/aui/latest/sandbox/)
directly or from within a flat pack download.

Atlassian Design Guidelines (ADG)
---------------------------------

The [ADG](https://developer.atlassian.com/design/) is built with AUI. It explains and demonstrates the design
components and principles behind the Atlassian UI. The Atlassian UX team developed the ADG. The team regularly updates
and usability tests the guidelines. Follow the ADG to leverage the work of experienced designers and make a beautiful
Atlassian UI.

Getting Started with AUI and ADG
--------------------------------

See the [AUI documentation](https://docs.atlassian.com/aui/latest/).

For a tutorial that teaches how to use AUI as either a flat pack or as an Atlassian plugin dependency, see
[Getting Started with AUI](https://developer.atlassian.com/display/AUI/Getting+Started+with+AUI).

Additional Documentation
---------------------------
* [Component documentation](https://docs.atlassian.com/aui/latest/)
* [Sandbox Tool](https://docs.atlassian.com/aui/latest/sandbox/)
* [Release Notes](https://developer.atlassian.com/display/AUI/AUI+Release+Notes)

Raising Issues
--------------

Raise bugs or feature requests in the [AUI project](https://ecosystem.atlassian.net/browse/AUI).

Master and Stable
-----------------

AUI maintains master and one or two stable branches.

Contributing to AUI
-------------------

Contributions to AUI are via pull request.

- Create an issue in the [AUI project](https://ecosystem.atlassian.net/browse/AUI). Creating an issue is a good place to
talk the AUI team about whether anyone else is working on the same issue, what the best fix is, and if this is a new feature,
whether it belongs in AUI. If you don't create an issue, we'll ask you to create one when you issue the PR and retag your
commits with the issue key.
- If you have write access to the AUI repo (ie if you work at Atlassian), you can create branches in the main AUI repo -
name your branch as `issue/{issue-key}-{description}`, eg `issue/AUI-1337-fix-the-contributor-guide`. If you don't have
write access, please fork AUI and issue a PR.
- Ensure all commits are tagged with the issue key (`AUI-1337 fixes to contributor guide`).
- Write tests. Unit tests are preferred over integration tests.
- Most PRs will go into master, however you might want them to go into a stable branch. If so, set the target branch
as the stable branch and the AUI team will manage merging stable into master after the PR is through.

If you are creating a new component, see [these additional guidelines](https://developer.atlassian.com/display/AUI/Creating+new+components).

[See here for instructions on setting up a dev environment](https://developer.atlassian.com/display/AUI/Setting+up+a+dev+environment+for+AUI).

Compatibility
-------------

AUI supports the following browsers:

- Chrome latest stable
- Firefox latest stable
- Safari latest stable (on OS X only)
- IE 8 / 9 / 10

AUI supports jQuery 1.7.2 and 1.8.3.

License
-------

See the [`licenses` directory](https://bitbucket.org/atlassian/aui/src/master/licenses/) for license information about AUI and included libraries.

FAQ
---

1. What version of AUI is running in my product?

To check the AUI version, open up a browser console and run `AJS.version`.