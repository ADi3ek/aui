module.exports = function(grunt) {
    'use strict';

    var INTEGRATION_PATH = 'integration';

    var spawn = require('child_process').spawn;

    grunt.registerMultiTask('install', 'Installs everything required for integration with Atlassian products.', function() {
        var done = this.async();
        var cmd = spawn('mvn', [
            'clean',
            'install',
            '-DskipTests',
            '-DskipAllPrompts=true',
            '-Djquery.version=' + grunt.config('jquery')
        ], {
            cwd: INTEGRATION_PATH
        });

        cmd.stderr.on('data', function(data) {
            grunt.log.error(data);
        });

        cmd.stdout.on('data', function(data) {
            grunt.log.writeln(data);
        });

        cmd.on('close', function(code) {
            done(code === 0);
        });
    });
};
