module.exports = function(grunt) {
    'use strict';

    var INTEGRATION_PATH = 'integration/refapp';

    var proc = require('child_process');

    grunt.registerMultiTask('refapp', 'Runs the refapp in a standalone server.', function() {
        var opts = this.options({
            debug: false
        });

        var cwd = process.cwd();
        var done = this.async();
        var debug = opts.debug ? 'debug' : 'run';
        var cmd = proc.spawn('mvn', [
            'amps:' + debug,
            '-DskipTests',
            '-DskipAllPrompts=true',
            '-Datlassian.disable.caches=true',
            '-Dplugin.resource.directories=../src',
            '-Djquery.version=' + grunt.config('jquery')
        ], {
            cwd: INTEGRATION_PATH
        });

        cmd.stderr.on('error', function(err) {
            grunt.log.error(err);
        });

        cmd.stdout.on('data', function(data) {
            grunt.log.writeln(data);
        });

        cmd.on('close', function(code) {
            done(code === 0);
        });
    });
};
