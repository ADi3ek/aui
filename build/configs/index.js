var fs = require('fs');
var path = require('path');

fs.readdirSync(__dirname).forEach(function(file) {
    var name = path.basename(file, '.js');
    module.exports[name] = require('./' + name);
});
