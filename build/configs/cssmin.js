module.exports = {
    aui: {
        files: {
            'dist/aui/aui-min.css': ['dist/aui/aui.css'],
            'dist/aui/aui-experimental-min.css': ['dist/aui/aui-experimental.css'],
            'dist/aui/aui-ie-min.css': ['dist/aui/aui-ie.css'],
            'dist/aui/aui-ie9-min.css': ['dist/aui/aui-ie.css'],
            'dist/aui/aui-all-min.css': ['dist/aui/aui-all.css']
        }
    },
    auiNext: {
        files: {
            'dist/aui-next/aui-min.css': ['dist/aui-next/aui.css'],
            'dist/aui-next/experimental-min.css': ['dist/aui-next/aui-experimental.css'],
            'dist/aui-next/aui-ie-min.css': ['dist/aui-next/aui-ie.css'],
            'dist/aui-next/aui-ie9-min.css': ['dist/aui-next/aui-ie.css']
        }
    }
};
