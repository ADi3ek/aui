module.exports = {
    options: {
        stripBanners: true,
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n\n'
    },
    aui: {
        files: {
            'dist/aui/aui.css': [
                'src/css/aui-reset.css',
                'src/css/aui-page-typography.css ',
                'src/css/html5.css',
                'src/css/aui-avatars.css',
                'src/css/aui-badge.css',
                'src/css/aui-buttons.css',
                'src/css/aui-date-picker.css',
                'src/css/aui-header.css',
                'src/css/aui-lozenge.css',
                'src/css/aui-navigation.css',
                'src/css/aui-page-layout.css ',
                'src/css/aui-page-header.css',
                'src/css/aui-toolbar2.css',
                'src/css/basic.css',
                'src/css/dialog.css',
                'src/css/layer.css',
                'src/css/dialog2.css',
                'src/css/dropdown.css',
                'src/css/dropdown2.css',
                'src/css/forms.css',
                'src/css/icons.css',
                'src/css/inline-dialog.css',
                'src/css/messages.css',
                'src/css/tables.css',
                'src/css/tabs.css',
                'src/css/toolbar.css'
            ],
            'dist/aui/aui-experimental.css': [
                'src/css/aui-module.css',
                'src/css/aui-experimental-labels.css',
                'src/css/aui-experimental-tables-sortable.css',
                'src/css/aui-iconfont.css',
                'src/css/aui-experimental-progress-tracker.css',
                'src/css-vendor/jquery/jquery.tipsy.css',
                'src/css/aui-experimental-tooltip.css',
                'src/css/aui-experimental-expander.css',
                'src/css/aui-experimental-progress-indicator.css',
                'src/css-vendor/jquery/plugins/jquery.select2.css',
                'src/css/aui-select2.css'
            ],
            'dist/aui/aui-ie.css': [
                'src/css/dialog-ie.css',
                'src/css/dropdown-ie.css',
                'src/css/forms-ie.css',
                'src/css/inline-dialog-ie.css',
                'src/css/toolbar-ie.css',
                'src/css/aui-toolbar2-ie.css',
                'src/css/aui-experimental-progress-tracker-ie.css'
            ],
            'dist/aui/aui-ie9.css': [
                'src/css/aui-badge-ie.css',
                'src/css/aui-header-ie.css',
                'src/css/inline-dialog-ie.css'
            ],
            'dist/aui/aui-all.css': [
                'dist/aui/aui.css',
                'dist/aui/aui-experimental.css'
            ],
            'dist/aui/aui.js': [
                'src/js-vendor/jquery/jquery-compatibility.js',
                'src/js-vendor/jquery/plugins/jquery.form.js',
                'src/js-vendor/jquery/plugins/jquery.aop.js',
                'src/js/raphael/raphael.shadow.js',
                'src/js/jquery/jquery.os.js',
                'src/js/jquery/jquery.hotkeys.js',
                'src/js/jquery/jquery.moveto.js',
                'src/js-vendor/jquery/jquery-ui/jquery.ui.datepicker.js',
                'src/js/atlassian.js',
                'src/js/format.js',
                'src/js/auiplugin.properties.js',
                'src/js/internal/browser.js',
                'src/js/internal/widget.js',
                'src/js/layer-manager.js',
                'src/js/layer-manager-global.js',
                'src/js/focus-manager.js',
                'src/js/context-path.cp.js',
                'src/js/cookie.js',
                'src/js/blanket.js',
                'src/js/layer.js',
                'src/js/dialog.js',
                'src/js/dialog2.js',
                'src/js/aui-date-picker.js',
                'src/js/dropdown.js',
                'src/js/dropdown2.js',
                'src/js/event.js',
                'src/js/firebug.js',
                'src/js/forms.js',
                'src/js/inline-dialog.js',
                'src/js/keyCode.js',
                'src/js/messages.js',
                'src/js/tabs.js',
                'src/js/template.js',
                'src/js/whenitype.js',
                'src/js/aui-header-responsive.js'
            ],
            'dist/aui/aui-dependencies.js': [
                'src/js-vendor/jquery/jquery.js',
                'src/js-vendor/raphael/raphael.js',
                'src/js-vendor/jquery/jquery-ui/jquery.ui.core.js',
                'src/js-vendor/jquery/jquery-ui/jquery.ui.widget.js',
                'src/js-vendor/jquery/jquery-ui/jquery.ui.mouse.js',
                'src/js-vendor/jquery/jquery-ui/jquery.ui.draggable.js',
                'src/js-vendor/jquery/jquery-ui/jquery.ui.sortable.js'
            ],
            'dist/aui/aui-experimental.js': [
                'src/js-vendor/jquery/jquery.tipsy.js',
                'src/js/aui-experimental-tooltip.js',
                'src/js/aui-experimental-tables-sortable.js',
                'src/js-vendor/jquery/jquery.tablesorter.js',
                'src/js/aui-experimental-expander.js',
                'src/js/aui-experimental-progress-indicator.js',
                'src/js-vendor/spin/spin.js',
                'src/js-vendor/jquery/jquery.spin.js',
                'src/js-vendor/jquery/plugins/jquery.select2.js',
                'src/js/aui-select2.js'
            ],
            'dist/aui/aui-ie.js': [
                'src/js-vendor/html5-shim/html5.js',
                'src/js/tables.js',
                'src/js/toolbar.js'
            ],
            'dist/aui/aui-soy.js': [
                'integration/plugin/target/soyutils/js/soyutils.js',
                'integration/plugin/target/qunit/soy/*.js'
            ],
            'dist/aui/aui-all.js': [
                'dist/aui/aui-dependencies.js',
                'dist/aui/aui.js',
                'dist/aui/aui-experimental.js',
                'dist/aui/aui-soy.js'
            ]
        }
    },
    auiNext: {
        files: {
            'dist/aui-next/aui-ie.js': [
                'dist/aui/aui-ie.js'
            ],
            'dist/aui-next/aui.js': [
                'src/js/jquery/jquery.os.js',
                'src/js/jquery/jquery.hotkeys.js',
                'src/js/jquery/jquery.moveto.js',
                'src/js/atlassian.js',
                'src/js/format.js',
                'src/js/auiplugin.properties.js',
                'src/js/internal/browser.js',
                'src/js/internal/widget.js',
                'src/js/layer-manager.js',
                'src/js/layer-manager-global.js',
                'src/js/focus-manager.js',
                'src/js/blanket.js',
                'src/js/layer.js',
                'src/js/dialog2.js',
                'src/js/cookie.js',
                'src/js/dialog.js',
                'src/js/dropdown2.js',
                'src/js/event.js',
                'src/js/forms.js',
                'src/js/inline-dialog.js',
                'src/js/keyCode.js',
                'src/js/messages.js',
                'src/js/tabs.js',
                'src/js/template.js',
                'src/js/whenitype.js',
                'src/js/aui-header-responsive.js'
            ],
            'dist/aui-next/aui-datepicker.js': [
                'src/js-vendor/jquery/jquery-ui/jquery.ui.core.js',
                'src/js-vendor/jquery/jquery-ui/jquery.ui.datepicker.js',
                'src/js/aui-date-picker.js'
            ],
            'dist/aui-next/aui-experimental.js': [
                'dist/aui/aui-experimental.js'
            ],
            'dist/aui-next/aui-soy.js': [
                'dist/aui/aui-soy.js'
            ],
            'dist/aui-next/aui.css': [
                'src/css/aui-reset.css',
                'src/css/aui-page-typography.css',
                'src/css/html5.css',
                'src/css/aui-avatars.css',
                'src/css/aui-badge.css',
                'src/css/aui-buttons.css',
                'src/css/aui-date-picker.css',
                'src/css/aui-header.css',
                'src/css/aui-lozenge.css',
                'src/css/aui-navigation.css',
                'src/css/aui-page-layout.css',
                'src/css/aui-page-header.css',
                'src/css/aui-toolbar2.css',
                'src/css/basic.css',
                'src/css/dialog.css',
                'src/css/layer.css',
                'src/css/dialog2.css',
                'src/css/dropdown2.css',
                'src/css/forms.css',
                'src/css/icons.css',
                'src/css/inline-dialog.css',
                'src/css/messages.css',
                'src/css/tables.css',
                'src/css/tabs.css'
            ],
            'dist/aui-next/aui-experimental.css': [
                'dist/aui/aui-experimental.css'
            ],
            'dist/aui-next/aui-ie.css': [
                'dist/aui/aui-ie.css'
            ],
            'dist/aui-next/aui-ie9.css': [
                'dist/aui/aui-ie9.css'
            ]
        }
    }
};
