module.exports = {
    aui: {
        files: {
          'dist/aui/aui-min.js': ['dist/aui/aui.js'],
          'dist/aui/aui-dependencies-min.js': ['dist/aui/aui-dependencies.js'],
          'dist/aui/aui-experimental-min.js': ['dist/aui/aui-experimental.js'],
          'dist/aui/aui-ie-min.js': ['dist/aui/aui-ie.js'],
          'dist/aui/aui-soy-min.js': ['dist/aui/aui-soy.js'],
          'dist/aui/aui-all-min.js': ['dist/aui/aui-all.js']
        }
    },
    auiNext: {
        files: {
          'dist/aui-next/aui-ie-min.js': ['dist/aui-next/aui-ie.js'],
          'dist/aui-next/aui-min.js': ['dist/aui-next/aui.js'],
          'dist/aui-next/aui-datepicker-min.js': ['dist/aui-next/aui-datepicker.js'],
          'dist/aui-next/aui-experimental-min.js': ['dist/aui-next/aui-experimental.js'],
          'dist/aui-next/aui-soy-min.js': ['dist/aui-next/aui-soy.js']
        }
    }
};
