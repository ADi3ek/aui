module.exports = {
    aui: {
        files: [{
            cwd: 'src/css',
            dest: 'dist/aui',
            expand: true,
            filter: 'isFile',
            flatten: true,
            src: ['fonts/**', 'images/**']
        }]
    },
    auiNext: {
        files: [{
            cwd: 'src/css',
            dest: 'dist/aui-next',
            expand: true,
            filter: 'isFile',
            flatten: true,
            src: ['fonts/**']
        }]
    }
};
