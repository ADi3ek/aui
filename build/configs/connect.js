module.exports = {
    docs: {
        options: {
            base: 'docs/target/docs',
            keepalive: true,
            open: true
        }
    }
};
