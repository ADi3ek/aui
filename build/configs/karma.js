module.exports = {
    options: require('./karma-shared'),
    cli: {
        options: {
            junitReporter: {
                outputFile: 'tests/karma-jquery-<%= jquery %>.xml'
            }
        }
    }
};
