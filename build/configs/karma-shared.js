module.exports = {
    singleRun: true,
    frameworks: ['qunit', 'requirejs', 'sinon'],
    browsers: ['PhantomJS'],
    reporters: ['progress', 'junit'],
    junitReporter: {
        outputFile: 'tests/karma.xml',
        suite: ''
    },
    files: [
        // karma + requireJS config
        '.tmp/tests-requirejs-config.js',
        'tests/karma-main.js',

        // CSS
        'src/css/layer.css',
        'src/css/tabs.css',
        'tests/atlassian-js/atlassian-js-test.css',

        // unpacked
        {pattern: 'integration/plugin/target/qunit/dependencies/jquery.js', included: false},
        {pattern: 'integration/plugin/target/soyutils/js/soyutils.js', included: false},
        {pattern: 'integration/plugin/target/qunit/soy/*.js', included: false},

        // main
        {pattern: 'src/**/*.js', included: false},

        // test
        {pattern: 'tests/aui-qunit.js', included: false},
        {pattern: 'tests/{*,**/*}-test.js', included: false}
    ]
};
