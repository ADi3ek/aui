module.exports = {
    options: {
        baseUrl: '/src/js',
        paths: {
            // Unpacked jquery
            'jquery': '../../integration/plugin/target/qunit/dependencies/jquery',

            // Unpacked soy dep
            'soyutils': '../../integration/plugin/target/soyutils/js/soyutils',

            // Compiled soy
            'soy/aui': '../../integration/plugin/target/qunit/soy/atlassian.soy',
            'soy/avatar': '../../integration/plugin/target/qunit/soy/avatar.soy',
            'soy/badges': '../../integration/plugin/target/qunit/soy/badges.soy',
            'soy/buttons': '../../integration/plugin/target/qunit/soy/buttons.soy',
            'soy/dialog2': '../../integration/plugin/target/qunit/soy/dialog2.soy',
            'soy/dropdown': '../../integration/plugin/target/qunit/soy/dropdown.soy',
            'soy/dropdown2': '../../integration/plugin/target/qunit/soy/dropdown2.soy',
            'soy/expander': '../../integration/plugin/target/qunit/soy/expander.soy',
            'soy/form': '../../integration/plugin/target/qunit/soy/form.soy',
            'soy/group': '../../integration/plugin/target/qunit/soy/group.soy',
            'soy/icons': '../../integration/plugin/target/qunit/soy/icons.soy',
            'soy/labels': '../../integration/plugin/target/qunit/soy/lables.soy',
            'soy/message': '../../integration/plugin/target/qunit/soy/message.soy',
            'soy/pane': '../../integration/plugin/target/qunit/soy/pane.soy',
            'soy/panel': '../../integration/plugin/target/qunit/soy/panel.soy',
            'soy/progress-tracker': '../../integration/plugin/target/qunit/soy/progress-tracker.soy',
            'soy/table': '../../integration/plugin/target/qunit/soy/table.soy',
            'soy/tabs': '../../integration/plugin/target/qunit/soy/tabs.soy',
            'soy/toolbar': '../../integration/plugin/target/qunit/soy/toolbar.soy',
            'soy/toolbar2': '../../integration/plugin/target/qunit/soy/toolbar2.soy',

            // Vendors
            'underscore': '../js-vendor/underscorejs/underscore',
            'backbone': '../js-vendor/backbone/backbone',
            'jquery.form': '../js-vendor/jquery/plugins/jquery.form',
            'jquery.aop': '../js-vendor/jquery/plugins/jquery.aop',
            'jquery.ui.datepicker': '../js-vendor/jquery/jquery-ui/jquery.ui.datepicker',
            'serializetoobject': '../js-vendor/jquery/serializetoobject',
            'html5-shim': '../js-vendor/html5-shim/html5',
            'jquery-select2': '../js-vendor/jquery/plugins/jquery.select2',

            // TODO: replace this with actual jquery ui deps
            'jquery-ui': '../js-vendor/jquery/jquery-ui/jquery-ui',

            // AUI
            'aui': 'atlassian',
            'blanket': 'blanket',
            'event': 'event',
            'dialog': 'dialog',
            'focus-manager': 'focus-manager',
            'layer-manager': 'layer-manager',
            'layer-manager-global': 'layer-manager-global',
            'keyCode': 'keyCode',
            'layer': 'layer',
            'dialog2': 'dialog2',
            'dropdown': 'dropdown',
            'inline-dialog': 'inline-dialog',
            'date-picker': 'aui-date-picker',
            'format': 'format',
            'events': 'experimental-events/events',
            'forms': 'forms',
            'template': 'template',
            'messages': 'messages',
            'select2': 'aui-select2',
            'sidebar': 'aui-experimental-sidebar',
            'tabs': 'tabs',
            'toolbar': 'toolbar',
            'jquery.hotkeys': 'jquery/jquery.hotkeys',
            'whenitype': 'whenitype',
            'autocomplete/progressive-data-set': 'experimental-autocomplete/progressive-data-set',
            'autocomplete/query-input': 'experimental-autocomplete/query-input',
            'autocomplete/query-result': 'experimental-autocomplete/query-result',
            'autocomplete/truncating-progressive-data-set': 'experimental-autocomplete/truncating-progressive-data-set',
            'internal/widget': 'internal/widget',
            'internal/browser': 'internal/browser',
            'restfultable': 'experimental-restfultable/restfultable',
            'restfultable/entrymodel': 'experimental-restfultable/restfultable.entrymodel',
            'restfultable/row': 'experimental-restfultable/restfultable.row',
            'restfultable/editrow': 'experimental-restfultable/restfultable.editrow',
            'restfultable/customview': 'experimental-restfultable/restfultable.customview',
            'restfultable/all': 'experimental-restfultable/restfultable.all',

            // test deps
            'aui-qunit': '../../tests/aui-qunit'
        },

        shim: {
            // vendor
            'backbone': ['jquery', 'underscore'],
            'jquery-ui': ['jquery'],
            'jquery.aop': ['jquery'],
            'jquery.hotkeys': ['jquery'],
            'serializetoobject': ['jquery'],
            'jquery-select2': ['jquery'],

            // aui
            'aui': ['jquery'],
            'blanket': ['aui'],
            'date-picker': ['aui', 'inline-dialog'],
            'dialog': ['aui', 'blanket', 'event'],
            'dialog2': ['aui', 'layer', 'layer-manager-global', 'internal/browser', 'internal/widget'],
            'dropdown': ['aui', 'jquery.aop'],
            'dropdown2': ['aui'],
            'event': ['aui'],
            'events': ['aui'],
            'format': ['aui'],
            'forms': ['aui'],
            'focus-manager': ['aui'],
            'inline-dialog': ['aui'],
            'keyCode': ['aui'],
            'layer': ['aui', 'focus-manager', 'internal/widget'],
            'layer-manager': ['aui', 'blanket', 'layer'],
            'layer-manager-global': ['aui', 'keyCode', 'layer-manager'],
            'messages': ['aui', 'template'],
            'select2': ['aui', 'jquery-select2'],
            'sidebar': ['aui', 'jquery-ui', 'underscore'],
            'tabs': ['aui'],
            'template': ['aui'],
            'toolbar': ['aui'],
            'whenitype': ['aui', 'format', 'keyCode', 'dropdown', 'jquery.hotkeys'],
            'internal/browser': ['aui'],
            'internal/widget': ['aui'],
            'autocomplete/progressive-data-set': ['aui', 'backbone'],
            'autocomplete/query-input': ['aui', 'backbone'],
            'autocomplete/query-result': ['aui', 'backbone'],
            'autocomplete/truncating-progressive-data-set': ['aui', 'autocomplete/progressive-data-set'],
            'restfultable': ['aui', 'backbone', 'events', 'format'],
            'restfultable/entrymodel': ['aui', 'backbone', 'events', 'restfultable'],
            'restfultable/row': ['aui', 'backbone', 'restfultable', 'soy/icons'],
            'restfultable/editrow': ['aui', 'backbone', 'restfultable', 'serializetoobject'],
            'restfultable/customview': ['aui', 'backbone', 'restfultable'],

            // restful table and its children have circular deps (restful table uses restfultable.*, restfultable.* requires
            // the restfultable namespace, so create a finaldep
            'restfultable/all': ['restfultable', 'restfultable/entrymodel', 'restfultable/row', 'restfultable/editrow', 'restfultable/customview'],

            // soy
            'soy/aui': ['soyutils'],
            'soy/avatar': ['soyutils', 'soy/aui'],
            'soy/badges': ['soyutils', 'soy/aui'],
            'soy/buttons': ['soyutils', 'soy/aui'],
            'soy/dialog2': ['soyutils', 'soy/aui'],
            'soy/dropdown': ['soyutils', 'soy/aui'],
            'soy/dropdown2': ['soyutils', 'soy/aui'],
            'soy/expander': ['soyutils', 'soy/aui'],
            'soy/form': ['soyutils', 'soy/aui'],
            'soy/group': ['soyutils', 'soy/aui'],
            'soy/icons': ['soyutils', 'soy/aui'],
            'soy/labels': ['soyutils', 'soy/aui'],
            'soy/message': ['soyutils', 'soy/aui'],
            'soy/pane': ['soyutils', 'soy/aui'],
            'soy/panel': ['soyutils', 'soy/aui'],
            'soy/progress-tracker': ['soyutils', 'soy/aui'],
            'soy/table': ['soyutils', 'soy/aui'],
            'soy/tabs': ['soyutils', 'soy/aui'],
            'soy/toolbar': ['soyutils', 'soy/aui'],
            'soy/toolbar2': ['soyutils', 'soy/aui'],

            // test
            'aui-qunit': ['aui']
        }
    }
};
