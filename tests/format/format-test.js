define(['format', 'aui-qunit'], function() {
    module("Format Unit Tests");

    test("AJS.Format with 1 parameter", function() {
        var testFormat = AJS.format("hello {0}", "world");
        equal(testFormat, "hello world", "expected AJS.format to return: 'hello world'" );
    });

    test("AJS.Format with 2 parameters", function() {
        var testFormat = AJS.format("hello {0} {1}", "world", "again");
        equal(testFormat, "hello world again", "expected AJS.format to return: 'hello world again'" );
    });

    test("AJS.Format with 3 parameters", function() {
        var testFormat = AJS.format("hello {0} {1} {2}", "world", "again", "!");
        equal(testFormat, "hello world again !", "expected AJS.format to return: 'hello world again !'" );
    });

    test("AJS.Format with 4 parameters", function() {
        var testFormat = AJS.format("hello {0} {1} {2} {3}", "world", "again", "!", "test");
        equal(testFormat, "hello world again ! test", "expected AJS.format to return: 'hello world again ! test'" );
    });

    test("AJS.Format with symbols", function() {
        var testFormat = AJS.format("hello {0}", "!@#$%^&*()");
        equal(testFormat, "hello !@#$%^&*()", "expected AJS.format to return: 'hello !@#$%^&*()'" );
    });

    test("AJS.Format with curly braces", function() {
        var testFormat = AJS.format("hello {0}", "{}");
        equal(testFormat, "hello {}", "expected AJS.format to return: 'hello {}'" );
    });

    test("AJS.Format with repeated parameters", function() {
        var testFormat = AJS.format("hello {0}, {0}, {0}", "world");
        equal(testFormat, "hello world, world, world", "expected AJS.format to return: 'hello world, world, world'" );
    });

    test("AJS.Format with apostrophe", function() {
        var testFormat = AJS.format("hello '{0}' {0} {0}", "world");
        equal(testFormat, "hello {0} world world", "expected AJS.format to return: 'hello {0} world world'" );
    });

    test("AJS.Format with very long parameters", function() {
        var testFormat = AJS.format("hello {0}", "this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long ");
        equal(testFormat, "hello this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long ", "expected AJS.format to return: 'hello this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long '" );
    });

    // choices

    test("AJS.Format with a choice value missing parameter", function() {
        var testFormat = AJS.format("We got {0,choice,0#|1#1 issue|1<{1,number} issues}");
        equal(testFormat, "We got ", "expected AJS.format to return: 'We got '" );
    });

    test("AJS.Format with a choice value with parameter lower first option", function() {
        var testFormat = AJS.format("We got {0,choice,0#0 issues|1#1|1<{1,number} issues}", -1, -1);
        equal(testFormat, "We got 0 issues", "expected AJS.format to return: 'We got 0 issues'" );
    });

    test("AJS.Format with a choice value first option", function() {
        var testFormat = AJS.format("We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}", 0);
        equal(testFormat, "We got 0 issues", "expected AJS.format to return: 'We got 0 issues'" );
    });

    test("AJS.Format with a choice value middle option", function() {
        var testFormat = AJS.format("We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}", 1);
        equal(testFormat, "We got 1 issue", "expected AJS.format to return: 'We got 1 issue'" );
    });

    test("AJS.Format with a choice value last option", function() {
        var testFormat = AJS.format("We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}", 2);
        equal(testFormat, "We got 2 issues", "expected AJS.format to return: 'We got 2 issues'" );
    });

    test("AJS.Format with a choice value with missing number parameter option", function() {
        var testFormat = AJS.format("We got {0,choice,0# |1#1 issue|1<{1,number} issues}", 2);
        equal(testFormat, "We got  issues", "expected AJS.format to return: 'We got  issues'" );
    });

    test("AJS.Format with a choice value with valid second option", function() {
        var testFormat = AJS.format("We got {0,choice,0# |1#1 issue|1<{1,number} issues}", 10, 10);
        equal(testFormat, "We got 10 issues", "expected AJS.format to return: 'We got 10 issues'" );
    });

    // number

    test("AJS.Format with a number value", function() {
        var testFormat = AJS.format("Give me {0,number}!", 5);
        equal(testFormat, "Give me 5!", "expected AJS.format to return: 'Give me 5!'");
    });

    test("AJS.Format with a number value", function() {
        var testFormat = AJS.format("Give me {0,number}!");
        equal(testFormat, "Give me !", "expected AJS.format to return: 'Give me !'");
    });

    module("I18n Unit Tests", {
        teardown: function() {
            if(AJS.I18n.keys) {
                delete AJS.I18n.keys;
            }
        }
    });

    test("AJS.I18n.getText return key test", function(){
        equal(AJS.I18n.getText("test.key"), "test.key");
    });

    test("AJS.I18n.getText return value test", function(){
        AJS.I18n.keys = {
            "test.key": "This is a Value"
        }
        equal(AJS.I18n.getText("test.key"), "This is a Value");
    });

    test("AJS.I18n.getText return value test", function(){
        AJS.I18n.keys = {
            "test.key": "Formatting {0}, and {1}"
        }
        equal(AJS.I18n.getText("test.key", "hello", "world"), "Formatting hello, and world");
    });
});