define(['dialog2', 'soy/dialog2', 'aui-qunit'], function() {
    module("Dialog2 integration tests", {
        setup: function() {
            sinon.spy(AJS, "dim");
            sinon.spy(AJS, "undim");
        },
        teardown: function() {
            AJS.dim.restore();
            AJS.undim.restore();
            // AJS.layer.show() moves the element to be a child of the body, so clean up popups
            AJS.$(".aui-layer").remove();
        }
    });

    test("Dialog2 focuses inside panel not on buttons", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            headerActionContent: "<button id='my-button'>Button</button>",
            content: "<input type='text' id='my-input' />"
        })).appendTo("#qunit-fixture");
        var buttonFocusSpy = sinon.spy();
        AJS.$("#my-button").focus(buttonFocusSpy);
        var inputFocusSpy = sinon.spy();
        AJS.$("#my-input").focus(inputFocusSpy);

        var dialog = AJS.dialog2($el);
        dialog.show();

        equal(buttonFocusSpy.callCount, 0, "Expected my-button to never be focused");
        ok(inputFocusSpy.calledOnce, "Expected my-input to be focused");
    });

    test("Dialog2 creates a blanket", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            headerActionContent: "<button id='my-button'>Button</button>",
            content: "<input type='text' id='my-input' />"
        })).appendTo("#qunit-fixture");

        var dialog = AJS.dialog2($el);
        dialog.show();

        ok(AJS.dim.calledOnce, "Expected blanket to be shown");
    });

    test("Dialog2 remove on hide false", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = AJS.dialog2($el);
        dialog.show();

        dialog.hide();

        ok(AJS.$.contains(document.documentElement, $el[0]))
    });

    test("Dialog2 remove on hide true", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "Hello world",
            removeOnHide: true
        })).appendTo("#qunit-fixture");
        var dialog = AJS.dialog2($el);
        dialog.show();

        dialog.hide();

        ok(!AJS.$.contains(document.documentElement, $el[0]))
    });

    test("Dialog2 global show event", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = AJS.dialog2($el);
        var spy = sinon.spy();
        AJS.dialog2.on("show", spy);

        dialog.show();

        ok(spy.calledOnce, "Expected event listener called");
        var $passedEl = spy.args[0][1];
        equal($passedEl[0], $el[0], "Expected first arg was dialog element");
    });

    test("Dialog2 global hide event", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = AJS.dialog2($el);
        var spy = sinon.spy();
        AJS.dialog2.on("hide", spy);
        dialog.show();

        dialog.hide();

        ok(spy.calledOnce, "Expected event listener called");
        var $passedEl = spy.args[0][1];
        equal($passedEl[0], $el[0], "Expected first arg was dialog element");
    });
});