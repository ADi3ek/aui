define(['dialog2', 'soy/dialog2', 'aui-qunit'], function() {
    module("Dialog2 soy tests");

    test("Dialog2 does creates close button for non-modal", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "hello world"
        }));

        ok($el.find(".aui-dialog2-header-close").length, "Expected a close button to be rendered");
    });

    test("Dialog2 does not create close button for modal", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "hello world",
            modal: true
        }));

        ok(!$el.find(".aui-dialog2-header-close").length, "Expected no close button to be rendered");
    });


    module("Dialog2 tests", {
        setup: function() {
            AJS._internal = {
                browser: {
                    supportsCalc: function() { return true; }
                }
            };
            sinon.stub(AJS, "layer", function($layerEl) {
                // This is added in createLayerMock() below
                return $layerEl.data("_ajs-test-layer-instance");
            })
        },
        // Creates a mock of a layer object. AJS.layer will return this when passed the given $el
        createLayerMock: function($el) {
            var layerInstance = {
                show: function() {},
                hide: function() {},
                remove: function() {},
                on: function() {}
            };
            $el.data("_ajs-test-layer-instance", layerInstance);
            return sinon.mock(layerInstance);
        },
        createContentEl: function() {
            return AJS.$(aui.dialog.dialog2({
                content: "Hello world"
            })).appendTo("#qunit-fixture");
        },
        teardown: function() {
            AJS.layer.restore();
        }
    });

    test("Dialog2 creates a dialog with given content", function() {
        var $el = this.createContentEl();

        var dialog = AJS.dialog2($el);

        equal($el[0], dialog.$el[0], "Dialog2 accepts given $el");
    });

    test("Dialog2 wraps layer for show, hide, remove", function() {
        expect(0);
        var $el = this.createContentEl();
        var dialog = AJS.dialog2($el);
        var layerMock = this.createLayerMock(dialog.$el);
        layerMock.expects("show").once();
        layerMock.expects("hide").once();
        layerMock.expects("remove").once();

        dialog.show();
        dialog.hide();
        dialog.remove();

        layerMock.verify();
    });

    test("Dialog2 hide is called on close button click", function() {
        expect(0);
        var $el = this.createContentEl();
        var $close = AJS.$("<div></div>").addClass("aui-dialog2-header-close").appendTo($el);
        var dialog = AJS.dialog2($el);
        var layerMock = this.createLayerMock(dialog.$el);
        layerMock.expects("hide").once();
        dialog.show();

        $close.click();

        layerMock.verify();
    });

    test("Dialog2 wraps layer events", function() {
        expect(0);
        var $el = this.createContentEl();
        var dialog = AJS.dialog2($el);
        var layerMock = this.createLayerMock(dialog.$el);
        var fn = function() {};
        layerMock.expects("on").once().withArgs("show", fn);

        dialog.on("show", fn);

        layerMock.verify();
    });

    test("Dialog2 does not call remove on hide when removeOnHide is false", function() {
        expect(0);
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide", false);
        var dialog = AJS.dialog2($el);
        var layerMock = this.createLayerMock(dialog.$el);
        layerMock.expects("remove").never();

        dialog.hide();

        layerMock.verify();
    });

    test("Dialog2 calls remove on hide when removeOnHide is true", function() {
        expect(0);
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide", true);
        var dialog = AJS.dialog2($el);
        var layerMock = this.createLayerMock(dialog.$el);
        layerMock.expects("remove").once();

        dialog.hide();

        layerMock.verify();
    });
});
