define(['forms', 'aui-qunit'], function() {
    module("Forms Unit Tests", {
        setup: function() {
            AJS.$('#qunit-fixture').html(
                '<form class="aui">' +
                    '<span id="fieldHelpToggle" class="aui-icon icon-inline-help"><span>Help</span></span>' +
                    '<span id="field-help" class="field-help hidden">Inline help text here.</span>' +
                '</form>'
            );
            AJS.inlineHelp();
        }
    });

    test("Test inlineHelp", function() {
        var fieldHelpToggle = AJS.$('#fieldHelpToggle');
        var fieldHelp = AJS.$('#field-help');

        ok(fieldHelp.hasClass('hidden'), "The fieldHelp element should have the hidden class");
        fieldHelpToggle.click();
        ok(!fieldHelp.hasClass('hidden'), "The fieldHelp element should not have the hidden class");
        fieldHelpToggle.click();
        ok(fieldHelp.hasClass('hidden'), "The fieldHelp element should have the hidden class again.");
    });
});