define(['autocomplete/progressive-data-set', 'aui-qunit'], function() {
    var testSearch = function(item, term) {
        return item.get("username") && item.get("username").indexOf(term) > -1;
    };

    module('querying with local data', {
        setup: function() {
            this.data = new AJS.ProgressiveDataSet([
                {name: "Chris", username: "cdarroch"},
                {name: "Jason", username: "jberry"},
                {name: "Adam", username: "aahmed"}
            ]);
            this.data.matcher = testSearch;
        }
    });

    test('empty query yields empty response', 3, function() {
        var callback = sinon.spy(), response;
        this.data.matcher = function() { return true; }
        this.data.bind('respond', callback);
        this.data.query('');
        response = callback.getCall(0).args[0];
        equal(typeof response, 'object', "callback returns object");
        equal(response.query, '', "includes query that was run");
        equal(response.results.length, 0, "should have no results");
    });

    test('can query the data it contains', 2, function() {
        var callback = sinon.spy(), response;
        this.data.bind('respond', callback);
        this.data.query('c');
        response = callback.getCall(0).args[0];
        equal(response.results.length, 1, "only one name has a 'c' in it"); // NOTE: I'm not implying the search type here, prefix or otherwise.
        equal(response.results[0].get("name"), "Chris");
    });

    test('queries result in an immediate response', 2, function() {
        var callback = sinon.spy();
        var data = new AJS.ProgressiveDataSet([], {
            queryEndpoint: false
        });
        data.bind('respond', callback);
        equal(callback.callCount, 0, "not called yet");
        data.query('');
        equal(callback.callCount, 1, "it got called");
    });

    test('can use #query as a callback handler', function() {
        var callback = sinon.spy();
        var trigger = function(fn) { fn.call(this, 'c'); }
        this.data.bind('respond', callback);
        trigger(this.data.query);

        equal(callback.callCount, 1, "it got called");
        equal(callback.getCall(0).args[0].results.length, 1, "it has a result");
    });

    module('configuring remote sources', {
        setup: function() {
            var requests = this.requests = [];
            this.xhr = sinon.useFakeXMLHttpRequest();
            this.xhr.onCreate = function(xhr) {
                requests.push(xhr);
            };
        }, teardown: function() {
            this.xhr.restore();
        }
    });

    test("can provide the name of the query parameter key", function() {
        var data = new AJS.ProgressiveDataSet([], {
            queryEndpoint: "/foo",
            queryParamKey: "bar"
        });
        data.query("myQuery");

        equal(this.requests[0].url, "/foo?bar=myQuery", "calling ProgressiveDataSet#url with a query string should generate the correct GET URL");
    });

    test("can provide additional query parameters to send with each query", function() {
        var data = new AJS.ProgressiveDataSet([], {
            queryEndpoint: "/foo",
            queryParamKey: "bar",
            queryData: {
                "maxResults": 10,
                "long": "john"
            }
        });
        data.query("myQuery");
        var url = this.requests[0].url;

        ok(/bar=myQuery/.test(url), "url contains the named query parameter");
        ok(/maxResults=10/.test(url), "url contains the 'maxResults' parameter");
        ok(/long=john/.test(url), "url contains the 'long' parameter");
    });

    test("query data can be a function", function() {
        var params = { "long": "john" };
        var queryData = function() { return params };
        var data = new AJS.ProgressiveDataSet([], {
            queryEndpoint: "/foo",
            queryParamKey: "bar",
            queryData: queryData
        });
        var url;

        data.query("myQuery");
        url = this.requests[0].url;

        ok(/long=john/.test(url), "url contains the 'long' parameter");

        params["maxResults"] = 10;
        data.query("myQuery");
        url = this.requests[1].url;

        ok(/maxResults=10/.test(url), "url contains the 'maxResults' parameter");
    });

    test("the named query parameter always trumps any additional query data provided", function() {
        var data = new AJS.ProgressiveDataSet([], {
            queryEndpoint: "/foo",
            queryParamKey: "bar",
            queryData: {
                "bar": "wrong"
            }
        });
        data.query("myQuery");
        var url = this.requests[0].url;

        ok(/bar=myQuery/.test(url), "the query is in the url");
        ok(!/bar=wrong/.test(url), "the additional metadata was clobbered");
    });

    module('querying with remote sources', {
        setup: function() {
            this.url = '/dummy/search';
            this.data = new AJS.ProgressiveDataSet([], {
                matcher: testSearch,
                maxResults: 5,
                queryEndpoint: this.url,
                queryParamKey: 'username'
            });
            this.server = sinon.fakeServer.create();
            this.server.respondWith("GET", this.url + "?username=a", [200, { "Content-Type": "application/json" }, '[{"username":"adam"},{"username":"admin"},{"username":"anna"}]']);
            this.server.respondWith("GET", this.url + "?username=c", [200, { "Content-Type": "application/json" }, '[{"username":"chris"}]']);
            this.server.respondWith("GET", this.url + "?username=j", [200, { "Content-Type": "application/json" }, '[{"username":"jason"}]']);
        }, teardown: function() {
            this.server.restore();
        }
    });

    test('queries result in an immediate (though perhaps incomplete) response, even when there is a remote source', 4, function() {
        var callback = sinon.spy();
        this.data.bind('respond', callback);
        equal(callback.callCount, 0, "not called yet");
        this.data.query('c');
        equal(callback.callCount, 1, "called immediately");
        var resp = callback.args[0][0];
        ok(_.isArray(resp.results), "immediate response has results");
        ok(_.isEmpty(resp.results), "response has nothing in it ('cause there's nothing in the data source)");
    });

    test('queries recieve a second response if the first is insufficient', function() {
        var callback = sinon.spy();
        this.data.bind('respond', callback);
        this.data.query('c');
        equal(callback.callCount, 1);

        this.server.respond();
        equal(callback.callCount, 2);

        var resp = callback.getCall(1).args[0];
        equal(resp.results.length, 1, "there should be one matching result from the server");
        equal(resp.results[0].get("username"), "chris", "the result should be the only one from the server that matches the search algorithm");
    });

    test("server isn't hit if there's no query", function() {
        this.data.query('');
        equal(this.server.requests.length, 0, "there shouldn't be a request to the server");
    })

    test('results from server are added to existing stored entries', function() {
        this.data.add({username:"bob"});
        this.data.query('a');
        this.server.respond();
        equal(this.data.length, 4, "there should be four results");
    });

    test('response always uses most recent query', function() {
        var callback = sinon.spy();

        // Make two queries that will result in ajax requests
        this.data.query('c');
        // TODO: some time passes...
        this.data.query('j');

        // bind to respond now, once the synchronous responses have already been made.
        this.data.bind('respond', callback);
        this.server.processRequest(this.server.requests[1]); // respond with the results for 'j' first,
        this.server.processRequest(this.server.requests[0]); // then 'c'.
        equal(callback.callCount, 2, "should be called twice; once for each response and subsequent updating of the data source");

        // assert results for the first response (on 'j')
        var resp = callback.getCall(0).args[0];
        equal(resp.query, "j", "the response was for the 'j' query");
        equal(resp.results.length, 1, "the response only contains one result");
        equal(resp.results[0].get("username"), "jason", "the response contained 'jason'");
        // assert the results were consistent between the 'j' and 'c' response
        ok(_.isEqual(callback.getCall(1).args, callback.getCall(0).args), "the second response (triggerd by searching for 'c') was the same as the first response");
    });

    test('does not re-request data when sufficient information available', function() {
        var callback = sinon.spy();
        var response = [200, {"Content-type": "application/json"}, '[{"username":"adam"},{"username":"admin"}]'];
        this.server.respondWith("GET", this.url + "?username=ad", response);
        this.server.respondWith("GET", this.url + "?username=adm", response);
        this.data.setMaxResults(2);
        this.data.bind('respond', callback);
        this.data.query('adm');
        equal(this.server.requests.length, 1);
        equal(callback.callCount, 1);

        this.server.respond();
        this.data.query('ad');

        equal(this.server.requests.length, 1, "we didn't need to make a new request");
        equal(callback.callCount, 3);
        equal(callback.getCall(1).args[0].results[0].get("username"), "admin");
        equal(callback.getCall(2).args[0].results[0].get("username"), "adam");
        equal(callback.getCall(2).args[0].results[1].get("username"), "admin");
    });

    test('requests are not re-made once they are returned', function() {
        this.data.query('a');
        this.data.query('c');
        this.server.respond();
        this.data.query('a');
        this.data.query('c');

        equal(this.server.requests.length, 2, "only two requests were made.");
    });

    module('remote activity', {
        setup: function() {
            var requests = this.requests = [];
            this.xhr = sinon.useFakeXMLHttpRequest();
            this.xhr.onCreate = function(xhr) {
                requests.push(xhr);
            };

            this.data = new AJS.ProgressiveDataSet([], {
                matcher: testSearch,
                queryEndpoint: '/dummy/search'
            });
            this.activity = sinon.spy();
            this.data.bind('activity', this.activity);
        }, teardown: function() {
            this.xhr.restore();
        }
    });

    test('fires an "activity" event when a server request is made', function() {
        this.data.query('a');

        ok(this.activity.calledOnce, 'an "activity" event was fired');
    });

    test('"activity" is true if there is an active server request', function() {
        this.data.query('a');

        deepEqual(this.activity.getCall(0).args[0], {activity: true}, 'should return an object that indicates activity is occurring');
    });

    test('fires an "activity" event when server responds', function() {
        this.data.query('a');
        this.requests[0].respond();

        ok(this.activity.calledTwice, 'a second "activity" event was fired on the server response');
    });

    test('"activity" is false when no active queries', function() {
        this.data.query('a');
        this.requests[0].respond();

        deepEqual(this.activity.getCall(1).args[0], {activity: false}, 'should return an object that indicates no activity');
    });

    test('"activity" is true when there is at least one active request', function() {
        this.data.query('arthos');
        this.data.query('porthos');
        this.requests[0].respond();

        equal(this.activity.callCount, 3, 'fired two times for each query, once for the response');
        deepEqual(this.activity.getCall(2).args[0], {activity: true}, "should still be active");
    });

    test('"activity" will not fire when a query was cached', function() {
        this.data.query('arthos');
        this.requests[0].respond();
        this.data.query('arthos');

        equal(this.activity.callCount, 2, 'called once for the request, once for the response.');
    });

    module('result sets', {
        dataSourceWithId: function(id) {
            var model = Backbone.Model.extend({ idAttribute: id });
            return new AJS.ProgressiveDataSet(null, { model: model });
        }
    });

    test('will happily duplicate data without a unique identifier', function() {
        var data = this.dataSourceWithId(false);
        data.add([{value:"abacus"},{value:"calculator"}]);
        data.add({value:"abacus"});
        equal(data.length, 3, "the ProgressiveDataSet should not guess at the equivalencies of the data");
    });

    test('can be given a uniqueness constraint', function() {
        var data = this.dataSourceWithId('value');
        data.add([{value:"abacus"},{value:"calculator"}]);
        data.add({value:"abacus"});
        equal(data.length, 2, "the ProgressiveDataSet's model dictates the uniqueness constraint");
    });


    module('result limits', {
        setup: function() {
            this.data = new AJS.ProgressiveDataSet();
            this.data.matcher = function() { return true; };
            for(var i=0,ii=20;i<ii;i++) {
                this.data.add({username:"chris"+i});
            }
        }
    });

    test('number of items in response can be limited', 1, function() {
        var callback = sinon.spy();
        this.data.setMaxResults(5);
        this.data.bind('respond', callback);
        this.data.query('c');
        equal(callback.getCall(0).args[0].results.length, 5, "should not respond with more than the max allowed results");
    });

    test('changing result limits causes response to be fired', function() {
        var callback = sinon.spy();
        this.data.bind('respond', callback);
        this.data.setMaxResults(5);
        this.data.query('c');
        this.data.setMaxResults(10);
        equal(callback.callCount, 2, "setting max results when there have been no queries should not trigger a response");
        equal(callback.getCall(0).args[0].results.length, 5, "querying data causes 5 results to be returned");
        equal(callback.getCall(1).args[0].results.length, 10, "changing the max results to 10 causes response to fire with 10 results");
    });
});
