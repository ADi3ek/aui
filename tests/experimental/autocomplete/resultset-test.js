define(['autocomplete/query-result', 'aui-qunit'], function() {
    module('result set - active element', {
        setup: function() {
            this.source = _.extend({}, Backbone.Events);
            this.resultset = new AJS.ResultSet({ source: this.source });
        }
    });

    test('is null when has no data', function () {
        var active = this.resultset.get('active');
        deepEqual(active, null, "should be no active model, as there are none in the set");
    });

    test('is the first model in the set when processed', function () {
        var models = [new Backbone.Model(), new Backbone.Model()];
        this.source.trigger('respond', { results: models });

        var active = this.resultset.get('active');
        deepEqual(active, models[0], "should be the first model in the set by default");
    });

    test('can be set by passing a model id', function () {
        var models = [ new Backbone.Model({id:"foo"}), new Backbone.Model({id:"bar"}), new Backbone.Model({id:"baz"}) ];
        this.source.trigger('respond', { results: models });

        var active = this.resultset.setActive("bar");
        deepEqual(active, models[1], "should be the middle model in the set");
    });

    test('is set to null if set doesn\'t contain the model id ', function () {
        var models = [ new Backbone.Model({id:"foo"}), new Backbone.Model({id:"bar"}), new Backbone.Model({id:"baz"}) ];
        this.source.trigger('respond', { results: models });

        var active = this.resultset.setActive("nonexistant");
        deepEqual(active, null, "id doesn't exist in set, so selected should now be null");
    });

    module('result set - collection', {
        setup: function() {
            this.source = _.extend({}, Backbone.Events);
            this.resultset = new AJS.ResultSet({ source: this.source });
        }
    });

    test('can iterate over the models in the set', 2, function() {
        var models = [new Backbone.Model(), new Backbone.Model()];
        this.source.trigger('respond', { results: models });

        this.resultset.each(function(model, index) {
            equal(model, models[index]);
        });
    });

    module('result set - empty set', {
        setup: function() {
            this.source = _.extend({}, Backbone.Events);
            this.resultset = new AJS.ResultSet({ source: this.source });
        }
    });

    test('#next and #prev set active to null', function() {
        this.resultset.next();
        deepEqual(this.resultset.get('active'), null);

        this.resultset.prev();
        deepEqual(this.resultset.get('active'), null);
    });


    module('result set - one element', {
        setup: function() {
            this.models = [ new Backbone.Model({id:"foo"}) ];
            this.source = _.extend({}, Backbone.Events);
            this.resultset = new AJS.ResultSet({ source: this.source });
            this.resultset.process({ query: "foo", results: this.models });
        }
    });

    test('#next and #prev don\'t affect active', function() {
        this.resultset.next();
        deepEqual(this.resultset.get('active'), this.models[0]);

        this.resultset.prev();
        deepEqual(this.resultset.get('active'), this.models[0]);
    });

    module('result set - two elements with first selected', {
        setup: function() {
            this.models = [ new Backbone.Model({id:"foo"}), new Backbone.Model({id:"fez"}) ];
            this.source = _.extend({}, Backbone.Events);
            this.resultset = new AJS.ResultSet({ source: this.source });
            this.resultset.process({ query: "f", results: this.models });
        }
    });

    test('#next will select the other element', function() {
        this.resultset.next();
        deepEqual(this.resultset.get('active'), this.models[1]);
    });

    test('#prev will select the other element', function() {
        this.resultset.prev();
        deepEqual(this.resultset.get('active'), this.models[1]);
    });

    function modelsEqual(actual, expected, reason) {
        deepEqual(actual.attributes, expected.attributes, reason);
    }

    module('result set - three elements with middle selected', {
        setup: function() {
            this.models = [ new Backbone.Model({id:"a1"}), new Backbone.Model({id:"a2"}), new Backbone.Model({id:"a3"}) ];
            this.source = _.extend({}, Backbone.Events);
            this.resultset = new AJS.ResultSet({ source: this.source });
            this.resultset.process({ query: "a", results: this.models });
            this.resultset.setActive("a2");
        }
    });

    test('calling #next once will select the third element', function() {
        this.resultset.next();
        modelsEqual(this.resultset.get('active'), this.models[2]);
    });

    test('calling #next twice will select the first element', function() {
        this.resultset.next();
        this.resultset.next();
        modelsEqual(this.resultset.get('active'), this.models[0]);
    });

    test('calling #next three times will select the second element', function() {
        this.resultset.next();
        this.resultset.next();
        this.resultset.next();
        modelsEqual(this.resultset.get('active'), this.models[1]);
    });

    test('calling #prev once will select the first element', function() {
        this.resultset.prev();
        modelsEqual(this.resultset.get('active'), this.models[0]);
    });

    test('calling #prev twice will select the third element', function() {
        this.resultset.prev();
        this.resultset.prev();
        modelsEqual(this.resultset.get('active'), this.models[2]);
    });

    test('calling #prev three times will select the second element', function() {
        this.resultset.prev();
        this.resultset.prev();
        this.resultset.prev();
        modelsEqual(this.resultset.get('active'), this.models[1]);
    });
});