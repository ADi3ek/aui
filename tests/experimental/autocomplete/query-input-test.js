define(['autocomplete/query-input', 'aui-qunit'], function() {
    module('query input - construction');

    test('attach element to query input', function () {
        var inputId = 'my-thing';
        var $input = $("<input/>", { 'id': inputId}).appendTo("#qunit-fixture");
        var queryInput = new AJS.QueryInput({ el: $input });
        equal(queryInput.$el.attr('id'), inputId);
    });

    module('query input - changing values', {
        setup: function() {
            var $input = $("<input/>").appendTo("#qunit-fixture");
            this.queryInput = new AJS.QueryInput({ el: $input });
        }
    })

    test('can retrieve input value', function () {
        equal(this.queryInput.val(), '', 'should return empty string when input has no value');
    });

    test('can set input value', function () {
        var myval = 'foo';
        this.queryInput.val(myval);

        equal(this.queryInput.val(), myval, 'should return "foo"');
    });

    test('can listen for changes to the input value', function() {
        var myval = 'foo';
        var callback = sinon.spy();

        this.queryInput.val(myval);
        this.queryInput.bind('change', callback);
        this.queryInput.changed();

        equal(callback.callCount, 1, "should be called");
        ok(callback.calledWith(myval), "should be passed 'foo'");
    });

    test('(BEHAVIOUR WILL CHANGE) by the time keyup fires, queryInput fires a change event', function() {
        var myval = 'foo';
        var callback = sinon.spy();

        this.queryInput.val(myval);
        this.queryInput.bind('change', callback);
        this.queryInput.$el.trigger('keyup');

        equal(callback.callCount, 1, "should be called");
        ok(callback.calledWith(myval), "should be passed 'foo'");
    });

    test('change event is only fired if the input actually changes', function() {
        var callback = sinon.spy();

        this.queryInput.bind('change', callback);
        this.queryInput.val('foo');
        this.queryInput.$el.trigger('keyup');
        this.queryInput.$el.trigger('keyup');

        equal(callback.callCount, 1, "should be called once only");
    });

    test('change event is fired if the input changes', function() {
        var callback = sinon.spy();

        this.queryInput.bind('change', callback);
        this.queryInput.val('foo');
        this.queryInput.$el.trigger('keyup');

        equal(callback.callCount, 1, "should be called once");

        this.queryInput.val('bar');
        this.queryInput.$el.trigger('keyup');

        equal(callback.callCount, 2, "should be called once again");
    });
});