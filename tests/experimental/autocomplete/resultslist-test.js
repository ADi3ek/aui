define(['autocomplete/progressive-data-set', 'autocomplete/query-result', 'aui-qunit'], function() {
    module('ResultsList', {
        setup: function() {
            this.ds = new AJS.ProgressiveDataSet();
            this.resultview = new AJS.ResultsList({ source: this.ds });
        }
    });

    test('#size', function() {
        this.ds.trigger('respond', { results: [new Backbone.Model()] });
        equal(this.resultview.size(), 1);

        this.ds.trigger('respond', { results: [] });
        equal(this.resultview.size(), 0);
    });

    test('clicking a result item triggers a "select" event', function () {
        var model = new Backbone.Model({id:"foo"});
        var spy = sinon.spy();

        this.resultview.bind('selected', spy);
        this.ds.trigger('respond', { query: 'foo', results: [model] });

        ok(!spy.called, "we haven't selected anything yet");

        this.resultview.$('li').first().trigger('click');

        ok(spy.calledOnce, "should fire the 'selected' event when a selection is made");
        ok(spy.calledWith(model), "the callback should be given our model");
    });

    module('ResultsList - visibility', {
        setup: function() {
            this.ds = _.extend({}, Backbone.Events);
            this.model = new Backbone.Model();
            this.resultview = new AJS.ResultsList({ source: this.ds });

            $("#qunit-fixture").append(this.resultview.$el);
        }
    })

    test('can be hidden programmatically', function() {
        ok(this.resultview.$el.is(":visible"), "should be visible");

        this.resultview.hide();
        ok(!this.resultview.$el.is(":visible"), "should hide the result list");
    });

    test('can be shown programmatically', function() {
        this.resultview.hide();
        this.resultview.show();
        ok(this.resultview.$el.is(":visible"), "should be visible");
    });

    test("not visible if the query hasn't changed since it was hidden", function() {
        var response = { query: 'foo', results: new Backbone.Model() };
        this.ds.trigger('respond', response);
        this.resultview.hide();
        this.ds.trigger('respond', response);
        ok(!this.resultview.$el.is(':visible'), 'should be hidden');
    });

    test("is visible if the query has changed since it was last hidden", function() {
        this.ds.trigger('respond', { query: 'foo', results: new Backbone.Model() });
        this.resultview.hide();
        this.ds.trigger('respond', { query: 'bar', results: new Backbone.Model() });
        ok(this.resultview.$el.is(':visible'), 'should be visible');
    });

    module('ResultsList#render', {
        setup: function() {
            this.ds = new AJS.ProgressiveDataSet();
            this.resultview = new AJS.ResultsList({ source: this.ds });
            this.resultview.render = sinon.spy(this.resultview, "render");
        }, teardown: function() {
            this.resultview.render.restore();
        }
    });

    test('is called when #show is called', function() {
        this.resultview.show();

        ok(this.resultview.render.calledOnce, "should #render when show is called");
    });

    test('is called when the source is queried', function () {
        this.ds.query('foo');

        ok(this.resultview.render.called, "should be called when respond is triggered");
    });

    test('is called even with an empty query for the source', function() {
        this.ds.query('');

        equal(this.resultview.render.callCount, 1, "we made an empty query, so we should render");
    });

    test("not called if the query hasn't changed since it was hidden", function() {
        var response = { query: 'foo', results: new Backbone.Model() };
        this.ds.trigger('respond', response);
        this.resultview.hide();
        this.ds.trigger('respond', response);
        ok(this.resultview.render.calledOnce, 'should only render once (for the first response)');
    });

    test("called if the query changes since it was last hidden", function() {
        this.ds.trigger('respond', { query: 'foo', results: new Backbone.Model() });
        this.resultview.hide();
        this.ds.trigger('respond', { query: 'bar', results: new Backbone.Model() });
        ok(this.resultview.render.calledTwice, 'should render twice (once for each different response)');
    });

    module("ResultsList#render - callbacks", {
        setup: function() {
            this.realRender = sinon.stub(AJS.ResultsList.prototype, "render");
            this.ds = new AJS.ProgressiveDataSet();
        }, teardown: function() {
            this.realRender.restore();
        }
    });

    test("fires events before and after it is called", function() {
        var view = new AJS.ResultsList({ source: this.ds });
        var before = sinon.spy();
        var after = sinon.spy();

        view.render = sinon.spy(view, "render");
        view.on('rendering', before);
        view.on('rendered', after);
        view.render();

        ok(before.called, "fires a 'rendering' event");
        ok(before.calledAfter(view.render), "the 'rendering' event fires just after the #render call");
        ok(before.calledBefore(this.realRender), "the 'rendering' event fires before the ResultsList#render function itself");

        ok(after.called, "fires a 'rendered' event");
        ok(after.calledAfter(view.render), "the 'rendered' event fires after the #render call");
        ok(after.calledAfter(this.realRender), "the 'rendered' event fires after the ResultsList#render function itself");
    });

    test("fires events even if ResultsList is extended", function() {
        var realRender = sinon.stub();
        var AnotherList = AJS.ResultsList.extend({ render: realRender });
        var view = new AnotherList({ source: this.ds });
        var before = sinon.spy();
        var after = sinon.spy();

        view.render = sinon.spy(view, "render");
        view.on('rendering', before);
        view.on('rendered', after);
        view.render();

        ok(before.called, "fires a 'rendering' event");
        ok(before.calledAfter(view.render), "the 'rendering' event fires just after the #render call");
        ok(before.calledBefore(realRender), "the 'rendering' event fires before the ResultsList#render function itself");

        ok(after.called, "fires a 'rendered' event");
        ok(after.calledAfter(view.render), "the 'rendered' event fires after the #render call");
        ok(after.calledAfter(realRender), "the 'rendered' event fires after the ResultsList#render function itself");
    });
});