define(['toolbar', 'aui-qunit'], function() {
    // Toolbar JS only loads for IE8 and below
    var isIE = ( AJS.$.browser.msie && parseInt(AJS.$.browser.version, 10) < 9 ),
        hasFirstClass,
        hasLastClass;

    module("Toolbar Unit Tests", {
        setup: function() {
            if (isIE) {
                AJS.setUpToolbars();
            }

            AJS.$('#qunit-fixture').html(
                '<div class="aui-toolbar" id="disabled-buttons-toolbar">' +
                    '<div class="toolbar-split toolbar-split-left">' +
                        '<ul class="toolbar-group">' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off (items)</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group">' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger disabled">Off (triggers)</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger disabled">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger disabled">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger disabled">Off</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group disabled">' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">Off (whole group)</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">Off</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group disabled">' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger disabled">Off (group, item and trigger)</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger disabled">Off</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger disabled">Off</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger disabled">Off</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group">' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Of</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group">' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group">' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group">' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group">' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group">' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                        '</ul>' +
                        '<ul class="toolbar-group">' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                            '<li class="toolbar-item"><a href="#" class="toolbar-trigger">On</a></li>' +
                            '<li class="toolbar-item disabled"><a href="#" class="toolbar-trigger">Off</a></li>' +
                        '</ul>' +
                    '</div>' +
                '</div>'
            );
        }
    });

    if (isIE) {

        test("Toolbar IE first classes", function() {
            hasFirstClass = AJS.$(".aui-toolbar .toolbar-group .toolbar-item:first-child").hasClass("first");
            equal( hasFirstClass, true, "First children should have class 'first'" );
        });

        test("Toolbar IE last classes", function() {
            hasLastClass = AJS.$(".aui-toolbar .toolbar-group .toolbar-item:last-child").hasClass("last");
            equal( hasLastClass, true, "Last children should have class 'last'" );
        });

    } else {

        test("Toolbar", function() {
            ok(true, "Toolbar has no testable javascript for this browser version");
        });

    }
});
