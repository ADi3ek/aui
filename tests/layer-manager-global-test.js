define(['layer-manager-global', 'aui-qunit'], function() {
    module("Layer manager global tests", {
        setup: function() {
            this.dimSpy = sinon.spy(AJS, "dim");
            this.undimSpy = sinon.spy(AJS, "undim");
            this.layerManagerPopTopSpy = sinon.stub(AJS.LayerManager.global, "popTop");
            this.layerManagerPopUntilTopBlanketedSpy = sinon.stub(AJS.LayerManager.global, "popUntilTopBlanketed");
        },
        teardown: function() {
        AJS.qunit.removeLayers();
            this.layerManagerPopTopSpy.restore();
            this.layerManagerPopUntilTopBlanketedSpy.restore();
            this.dimSpy.restore();
            this.undimSpy.restore();
        },
        createLayer: function(blanketed) {
            var $el = AJS.$("<div></div>").addClass("aui-layer").attr("aria-hidden", "true").css("position", "absolute").appendTo("#qunit-fixture");
            if (blanketed) {
                $el.attr("data-aui-blanketed", "true");
            }
            return $el;
        },
        pressEsc: function() {
            var e = jQuery.Event("keydown");
            e.keyCode = AJS.keyCode.ESCAPE;
            $(document).trigger(e);
        },
        clickBlanket: function() {
            // We don't want to include blanket.js - which creates the blanket - in our dependencies,
            // so create a mock blanket element
            var $blanket = AJS.$("<div></div>").addClass("aui-blanket").appendTo("#qunit-fixture");
            $blanket.click();
        }
    });

    test("Pressing ESC calls popTop", function() {
        var $el = this.createLayer();
        AJS.LayerManager.global.push($el);

        this.pressEsc();

        ok(this.layerManagerPopTopSpy.calledOnce, "Expected popUntil called once");
    });

    test("Clicking blanket calls popUntilTopBlanketed", function() {
        var $el = this.createLayer(true);
        AJS.LayerManager.global.push($el);

        this.clickBlanket();

        ok(this.layerManagerPopUntilTopBlanketedSpy.calledOnce, "Expected popUntil called once");
    });
});