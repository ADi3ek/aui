define(['tabs', 'soy/tabs', 'aui-qunit'], function() {
    module("Tabs Unit Tests", {
        renderTemplate: function(c) {
            // TODO: render as soy ... (bitbucket.org/atlassian/soy-to-html-plugin ?)
            $("#qunit-fixture").append([
                "<" + (c.tagName || "div") + " class='aui-tabs horizontal-tabs'>",
                "  <ul class='tabs-menu'>",
                "    <li class='menu-item active-tab'>",
                "      <a href='#tab-lister' id='menu-item-lister'><strong>Lister</strong></a>",
                "    </li>",
                "    <li class='menu-item'>",
                "      <a href='#tab-rimmer' id='menu-item-rimmer'><strong>Rimmer</strong></a>",
                "    </li>",
                "  </ul>",
                "  <" + (c.paneTagName || "div") + " id='tab-lister' class='tabs-pane active-pane'>",
                "    +1",
                "  </" + (c.paneTagName || "div") + ">",
                "  <" + (c.paneTagName || "div") + " id='tab-rimmer' class='tabs-pane'>",
                "    -1",
                "  </" + (c.paneTagName || "div") + ">",
                "</" + (c.tagName || "div") + ">"
            ].join(""));
        },
        addTabMenu: function() {
            // TODO: use soy
            $(".tabs-menu").append([
                "    <li class='menu-item'>",
                "      <a href='#tab-kryten' id='menu-item-kryten'><strong>Kryten</strong></a>",
                "    </li>"
            ].join(""));
        },
        addTabPane: function(c) {
            // TODO: use soy
            $(".aui-tabs").append([
                "  <" + (c.paneTagName || "div") + " id='tab-kryten' class='tabs-pane'>",
                "    +∞",
                "  </" + (c.paneTagName || "div") + ">"
            ].join(""));
        }
    });

    test("Tabs API Test", function() {
           ok(typeof AJS.tabs == "object", "AJS.tabs exists!");
           ok(typeof AJS.tabs.setup == "function", "AJS.tabs.setup function exists!");
           ok(typeof AJS.tabs.change=="function", "AJS.tabs.change function exists!");
    });

    test("Clicking tab label hides active tab and shows new tab", function() {
        this.renderTemplate({});
        AJS.tabs.setup();

        $("#menu-item-rimmer").trigger("click");

        ok(!$("#tab-lister").is(":visible"), "Expected first tab should be hidden");
        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");
    });

    test("Tabs work when constructed with non-div tags", function() {
        this.renderTemplate({
            tagName: "section",
            paneTagName: "section"
        });
        AJS.tabs.setup();

        $("#menu-item-rimmer").trigger("click");

        ok(!$("#tab-lister").is(":visible"), "Expected first tab should be hidden");
        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");
    });

    test("Calling AJS.tabs.setup() multiple times does not double-bind event handlers", function() {
        this.renderTemplate({});
        AJS.tabs.setup();
        AJS.tabs.setup();
        var counter = 0;
        $(".tabs-menu a").bind("tabSelect", function() {
            ++counter;
        });
        $("#menu-item-rimmer").trigger("click");
        equal(counter, 1, "Expected change handler invoked only once");
    });

    test("Tabs should work if you add a tab after calling AJS.tabs.setup", function() {
        this.renderTemplate({});
        AJS.tabs.setup();
        this.addTabMenu();
        this.addTabPane({});

        $("#menu-item-kryten").trigger("click");

        ok(!$("#tab-lister").is(":visible"), "Expected first tab should be hidden");
        ok(!$("#tab-rimmer").is(":visible"), "Expected second tab should be hidden");
        ok($("#tab-kryten").is(":visible"), "Expected third tab should be visible");
    });

    test("Tabs soy template exists", function() {
        ok(aui.tabs);
    });

    test("Tabs soy template basic rendering", function() {
        var html = aui.tabs({
            menuItems : [{
            isActive : true,
            url : '#tab1',
            text : 'Tab 1'
        }, {
            url : '#tab2',
            text : 'Tab 2'
        }],
        paneContent : aui.tabPane({
            isActive : true,
            content : 'Tab 1 Content'
            }) + aui.tabPane({
                content : 'Tab 2 Content'
            })
        });

        var $el = $(html).appendTo(AJS.$("#qunit-fixture"));
        ok($el.is(".aui-tabs"));
    });
});